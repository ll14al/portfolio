import numpy as np
import matplotlib.pyplot as plt
import math as mth

# w = weight matrix , h = hidden layer height, eta = learning rate

w=0 # declaration for global accesabillity and modification  

def plot_save_data(data1,data2,data3,data4):

	fig = plt.figure(); ax2 = fig.add_subplot(1, 1, 1)
	ax2.scatter(data1[:,0], data1[:,1],marker='+',linewidths=1)
	ax2.scatter(data2[:,0], data2[:,1],marker='+',linewidths=1)
	ax2.scatter(data3[:,0], data3[:,1],marker='.',linewidths=1)
	ax2.scatter(data4[:,0], data4[:,1],marker='.',linewidths=1)
	ax2.set_xlim([-10,10]),ax2.set_ylim([-10,10])
	ax2.grid(True, which='both')
	ax2.axhline(y=0, color='k')
	ax2.axvline(x=0, color='k')
	fig.savefig('dataset.png') 
	#plt.show()
	plt.close()

def plot_training_against_validation(errordata,filename):
	fig, ax = plt.subplots()
	line1,= ax.plot(errordata[:,0],errordata[:,1],label='validation')
	line2,= ax.plot(errordata[:,0],errordata[:,2],label='training')
	ax.legend(loc='upper right')
	fig.savefig(filename) 
	plt.show()

def normalize_data(D):
	xmean=np.mean(D[:,0])
	ymean=np.mean(D[:,1])
	xstd=np.std(D[:,0])
	ystd=np.std(D[:,1])
	
	for i in range(D.shape[0]):
		D[i,0]=(D[i,0]-xmean)/xstd
		D[i,1]=(D[i,1]-ymean)/ystd
	
	return D
	
def generate_dataset():
	#training = [1000,3], validation = [500,3], test = [500,3]
	
	# rectangle 1: bottom left corner 1,-3
	#			 : 4x3
	
	# rectangle 2: bottom left corner 3,2 
	#			 : 3x4
	rx= np.random.uniform( 1,5,((500,1)))
	ry= np.random.uniform(-3,0,((500,1)))
	data1 =np.append(rx,ry,axis=1)
	rx= np.random.uniform( 3,6,((500,1)))
	ry= np.random.uniform( 2,6,((500,1)))
	data2 =np.append(rx,ry,axis=1)
	
	#Rotate rectangles
	angles = np.matrix([[mth.cos(0.488692),mth.sin(-0.488692) ], 
						[mth.sin(0.488692),mth.cos(0.488692) ]])
	
	for i in range(500):
		data1[i]=data1[i]*angles
		data2[i]=data2[i]*angles
	
	#Creating 2 sets of data distributions 
	m1=np.array([-4,-5]); cov1=np.matrix([[1,0],[0,1]])
	m2=np.array([-2,-2]); cov2=np.matrix([[-1,0],[0,-2]])

	data3= np.random.multivariate_normal(m1, cov1,(500))
	data4= np.random.multivariate_normal(m2, cov2,(500))

	plot_save_data(data1,data2,data3,data4)
	
	# Add classification to data 
	clss = np.zeros((500,1));clss.fill(1)
	data1 = np.append(data1,clss,axis=1);clss.fill(2)
	data2 = np.append(data2,clss,axis=1);clss.fill(3)
	data3 = np.append(data3,clss,axis=1);clss.fill(4)
	data4 = np.append(data4,clss,axis=1)
	
	#Group together and shuffle 
	grouped = np.concatenate((data1,data2,data3,data4),axis=0)
	np.random.shuffle(grouped)
	
	
	#Writing data set to file
	np.savetxt('data.txt',grouped, delimiter=',',newline='\n')
		
	#Split data for differing uses
	training = grouped[0:1000,:]
	validation = grouped[1000:1500,:]
	test = grouped[1500:,:]
	
	return training, validation, test


def classify_mlp(h,x): # weights, hidden layer amount, 1 data point 	
	
	# add -1 to point vector for bias
	zi=np.append(x,[[-1]],axis=1) 
	
	#calculate sums of inputs to hidden layer neurons 
	# and insert -1 to vector for bias
	zj=np.append((1/(1+np.exp(-(x*w[:x.shape[1],:h])))),[[-1]],axis=1)
	
	#calculate sums of inputs to ouput-layer neurons
	#returns: 
	#class label of highest g(Zk) vector member, 
	#zk,zj,zi(aka input point)
	return ((np.argmax(1/(1+np.exp(-(zj*w[:,h:]))))+1),
		   (1/(1+np.exp(-(zj*w[:,h:])))), zj, zi)
		   
def class_to_vector(c):
	if c == 1:
		return(np.matrix([[1,0,0,0]]))
	if c == 2:
		return(np.matrix([[0,1,0,0]]))
	if c == 3:
		return(np.matrix([[0,0,1,0]]))
	if c == 4:
		return(np.matrix([[0,0,0,1]]))


def train_on_point(D,h,eta):
	global w 
	x=np.matrix([[D[0],D[1]]])
	c,zk,zj,zi=classify_mlp(h,x)
	tk=class_to_vector(D[2])
	
	dk=np.multiply((zk-tk),np.multiply(zk,(1-zk)))
	
	sumwjkdk=np.sum(np.multiply(w[:,h:],dk),axis=1)
	dj=np.multiply(zj[:,:h],(np.multiply((1-zj[:,:h]),sumwjkdk.T[:,1:])))
	
	w[:,h:]-= eta*(np.outer(zj,dk))
	w[:3,:h]-= eta*(np.outer(zi,dj))
	
	
def train_mlp(h,eta,D):
	
	np.apply_along_axis(train_on_point,1,D,h,eta)

def confusion_matrix(D,h):
	cm=np.zeros((5,4))
	for i in range(D.shape[0]):
		x=np.matrix([[D[i,0],D[i,1]]])
		guess= classify_mlp(h,x)
		cm[int((D[i,2]-1)),(guess[0]-1)]+=1
	
	diagv=np.zeros((5,1))
	for i in range(4):
		diagv[i]=round(cm[i,i]/(np.sum(cm[i,:]))*100,2)
	
	cm = np.append(cm,diagv,axis=1)
	cm[4,4]=round(np.sum(cm[:,4])/4,2)
	return cm
	
def evaluate_mlp(h,D):

	cm = confusion_matrix(D,h)

	accuracy = cm[-1,-1]
	precision = [cm[0,-1],cm[1,-1],cm[2,-1],cm[3,-1]]
	
	recall = [0,0,0,0] # with divide by zero guards
	recall[0]= -1 if np.sum(cm[:,0])==0 else int(cm[0,0])/int(np.sum(cm[:,0]))
	recall[1]= -1 if np.sum(cm[:,1])==0 else int(cm[1,1])/int(np.sum(cm[:,1])) 
	recall[2]= -1 if np.sum(cm[:,2])==0 else int(cm[2,2])/int(np.sum(cm[:,2]))
	recall[3]= -1 if np.sum(cm[:,3])==0 else int(cm[3,3])/int(np.sum(cm[:,3]))
	
	
	sensitivity = [0,0,0,0] # with divide by zero guards
	sensitivity[0]= -1 if np.sum(cm[:,0])==0 else int(cm[0,0])/int(np.sum(cm[:,0]))
	sensitivity[1]= -1 if np.sum(cm[:,1])==0 else int(cm[1,1])/int(np.sum(cm[:,1]))
	sensitivity[2]= -1 if np.sum(cm[:,2])==0 else int(cm[2,2])/int(np.sum(cm[:,2]))
	sensitivity[3]= -1 if np.sum(cm[:,3])==0 else int(cm[3,3])/int(np.sum(cm[:,3]))
	
	
	specificity = [0,0,0,0]  # with divide by zero guards
	TN1 = int(np.sum(cm[1:-1,1:-1]))
	FP1 = np.sum(cm[0,1:-1])
	specificity[0] = -1 if int(TN1+FP1)==0 else int(TN1)/int(TN1+FP1)
	
	TN2 = (np.sum(cm[0,:-1])-cm[0,1])+np.sum(cm[2:,0])+np.sum(cm[2:-1,2:-1])
	FP2 = cm[0,1]+cm[2,1]+cm[3,1]
	specificity[1] = -1 if int(TN2+FP2)==0 else int(TN2)/int(TN2+FP2)
	
	TN3 = (np.sum(cm[:2,:-1])-cm[0,2]-cm[1,2])+(np.sum(cm[3,:-1])-cm[2,2])
	FP3 = cm[2,0]+cm[2,1]+cm[2,3]
	specificity[2] = -1 if int(TN2+FP2)==0 else int(TN2)/int(TN2+FP2)
	
	TN4 = np.sum(cm[:-2,:-2])
	FP4 = cm[0,3]+cm[1,3]+cm[2,3]
	specificity[3] = -1 if int(TN3+FP3)==0 else int(TN3)/int(TN3+FP3)
	
	return cm, accuracy, precision, recall, sensitivity, specificity

def print_evaluation(cm, accuracy, prec, recall, sens, spec,h,eta,epochs):
	print("Evaluation of MLP")
	print("with height of "+str(h)+", learning rate of "+str(eta)+" over "+str(epochs)+" epochs.")
	print("confusion matrix 4x4")
	print(str(cm)+"\n")
	print("Accuracy = "+str(accuracy)+"\n")
	print("Class 1 Predictions")
	print("prec = "+str(round(prec[0],2))+", recall = "+str(round(recall[0],2))+
		 ", sens = "+str(round(sens[0],2))+", spec = "+str(round(spec[0],2)))
	print("Class 2 Predictions")
	print("prec = "+str(round(prec[1],2))+", recall = "+str(round(recall[1],2))+
		 ", sens = "+str(round(sens[1],2))+", spec = "+str(round(spec[1],2)))
	print("Class 3 Predictions")
	print("prec = "+str(round(prec[2],2))+", recall = "+str(round(recall[2],2))+
		 ",sens = "+str(round(sens[2],2))+", spec = "+str(round(spec[2],2)))
	print("Class 4 Predictions")
	print("prec = "+str(round(prec[3],2))+", recall = "+str(round(recall[3],2))+
		 ", sens = "+str(round(sens[3],2))+", spec = "+str(round(spec[3],2)))
	
	
def network_size_exp(training,validation, test):
	
	eta=0.01; epochs=500
	global w
	

	h=2; # test case 1 	
	w=np.random.uniform(-0.5,0.5,((h+1,h+4))) 
	acc = evaluate_mlp(h,test)
	print("1. Hidden layer height 2:")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	
	for i in range(epochs):
		np.random.shuffle(training)
		train_mlp(h,eta,training)
		
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)
	
	
	h=5 #test case 2
	w=np.random.uniform(-0.5,0.5,((h+1,h+4))) 
	acc = evaluate_mlp(h,test)
	print("\n\n2. Hidden layer height 5:")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	
	for i in range(epochs):
		np.random.shuffle(training)
		train_mlp(h,eta,training)
		
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)

	h = 30 # test case 3
	w=np.random.uniform(-0.5,0.5,((h+1,h+4))) 
	acc = evaluate_mlp(h,test)
	print("\n\n3. Hidden layer height 30:")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	
	for i in range(epochs):
		np.random.shuffle(training)
		train_mlp(h,eta,training)
		
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)
	
	h = 100 # test case 4
	w=np.random.uniform(-0.5,0.5,((h+1,h+4))) 
	acc = evaluate_mlp(h,test)
	print("\n\n4. Hidden layer height 100:")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	
	for i in range(epochs):
		np.random.shuffle(training)
		train_mlp(h,eta,training)
		
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)


def learning_rate_exp(training,validation,test):
	h=100; epochs=500
	global w
	w=np.random.uniform(-0.5,0.5,((h+1,h+4)))
	backupw = np.matrix(w) # used to store the same weights to start each experiment from
	
	
	eta=1; # TEST CASE 1 
	acc = evaluate_mlp(h,test)
	print("\n\n1. learning rate = 1")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	errordata=np.zeros((epochs+1,3))
	errordata[0,0] = 0 
	ev=evaluate_mlp(h,validation);errordata[0,1] = 100-ev[1]
	et= evaluate_mlp(h,training); errordata[0,2] = 100-et[1]
	
	for i in range(epochs):
		np.random.shuffle(training)
		errordata[i+1,0] = i+1
		train_mlp(h,eta,training)
		ev=evaluate_mlp(h,validation);errordata[i+1,1] = 100-ev[1]
		et= evaluate_mlp(h,training); errordata[i+1,2] = 100-et[1]
		
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)
	plot_training_against_validation(errordata,"eta_1")

	eta=0.2; # TEST CASE 2
	w = np.matrix(backupw) # reset weights to initialy generated
	acc = evaluate_mlp(h,test)
	print("\n\n2. learning rate = 0.1")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	errordata=np.zeros((epochs+1,3))
	errordata[0,0] = 0; 
	ev=evaluate_mlp(h,validation);errordata[0,1] = 100-ev[1]
	et= evaluate_mlp(h,training); errordata[0,2] = 100-et[1]
	
	for i in range(epochs):
		np.random.shuffle(training)
		errordata[i+1,0] = i+1
		train_mlp(h,eta,training)
		ev=evaluate_mlp(h,validation);errordata[i+1,1] = 100-ev[1]
		et= evaluate_mlp(h,training) ;errordata[i+1,2] = 100-et[1]
		
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)
	plot_training_against_validation(errordata,"eta_2")
	
	eta=0.01; # TEST CASE 3
	w = np.matrix(backupw) # reset weights to initialy generated
	acc = evaluate_mlp(h,test)
	print("\n\n3. learning rate = 0.01")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	errordata=np.zeros((epochs+1,3))
	errordata[0,0] = 0; 
	ev=evaluate_mlp(h,validation);errordata[0,1] = 100-ev[1]
	et= evaluate_mlp(h,training); errordata[0,2] = 100-et[1]
	
	for i in range(epochs):
		np.random.shuffle(training)
		errordata[i+1,0] = i+1
		train_mlp(h,eta,training)
		ev=evaluate_mlp(h,validation);errordata[i+1,1] = 100-ev[1]
		et= evaluate_mlp(h,training) ;errordata[i+1,2] = 100-et[1]
	
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)
	plot_training_against_validation(errordata,"eta_3")
	
	eta=0.001; # TEST CASE 4
	w = np.matrix(backupw) # reset weights to initialy generated 
	acc = evaluate_mlp(h,test)
	print("\n\n4. Hidden layer height = 0.001")
	print(" Accuracy before training = "+str(acc[1]))
	print(" Precision before training = "+str(acc[2]))
	errordata=np.zeros((epochs+1,3))
	errordata[0,0] = 0; 
	ev=evaluate_mlp(h,validation);errordata[0,1] = 100-ev[1]
	et= evaluate_mlp(h,training); errordata[0,2] = 100-et[1]
	
	for i in range(epochs):
		np.random.shuffle(training)
		errordata[i+1,0] = i+1
		train_mlp(h,eta,training)
		ev=evaluate_mlp(h,validation);errordata[i+1,1] = 100-ev[1]
		et= evaluate_mlp(h,training) ;errordata[i+1,2] = 100-et[1]
	
	cm,a,p,r,sn,sp = evaluate_mlp(h,test)
	print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)
	plot_training_against_validation(errordata,"eta_4")
	
	plt.close('all')
	
def best_predictions_data_plot(test):
	h=100; epochs=500; eta=0.01
	global w
	w=np.random.uniform(-0.5,0.5,((h+1,h+4))) 
	
	for i in range(epochs):
		np.random.shuffle(training)
		train_mlp(h,eta,training)
	
	class1 = np.zeros((1,2))
	class2 = np.zeros((1,2))
	class3 = np.zeros((1,2))
	class4 = np.zeros((1,2))
	
	for i in range(test.shape[0]):
		x=np.matrix([[test[i,0],test[i,1]]])
		c,zk,zj,zi=classify_mlp(h,x)
		record = np.zeros((1,2))
		
		if c == 1:
			record[0,0] = test[i,0]
			record[0,1] = test[i,1]
			class1 = np.append(class1,record,axis=0)
		if c == 2:
			record[0,0] = test[i,0]
			record[0,1] = test[i,1]
			class2 = np.append(class2,record,axis=0)
		if c == 3:
			record[0,0] = test[i,0]
			record[0,1] = test[i,1]
			class3 = np.append(class3,record,axis=0)
		if c == 4:
			record[0,0] = test[i,0]
			record[0,1] = test[i,1]
			class4 = np.append(class4,record,axis=0)	
	
	fig = plt.figure(); ax2 = fig.add_subplot(1, 1, 1)
	ax2.scatter(class1[:,0], class1[:,1],marker='+',linewidths=1)
	ax2.scatter(class2[:,0], class2[:,1],marker='+',linewidths=1)
	ax2.scatter(class3[:,0], class3[:,1],marker='+',linewidths=1)
	ax2.scatter(class4[:,0], class4[:,1],marker='+',linewidths=1)
	ax2.set_xlim([-10,10]),ax2.set_ylim([-10,10])
	ax2.grid(True, which='both')
	ax2.axhline(y=0, color='k')
	ax2.axvline(x=0, color='k')
	fig.savefig('predictions.png') 
	plt.show()
	plt.close()
		

training, validation, test = generate_dataset()
#network_size_exp(training, validation, test)
learning_rate_exp(training, validation, test)
best_predictions_data_plot(test)




#Initializing weights to random numbers between [-0.5,0.5]
h=100
w=np.random.uniform(-0.5,0.5,((h+1,h+4))) 
eta=0.01; epochs=10
eval=evaluate_mlp(h,test)

#error=0
#errordata=np.zeros((epochs,3))

for i in range(epochs):
	np.random.shuffle(training)
	print("#"+str(i)+" epoch")
	train_mlp(h,eta,training)
	#errordata[i,0] = i
	#errordata[i,1] = evaluate_mlp(h,validation)
	#errordata[i,2] = evaluate_mlp(h,training)

cm,a,p,r,sn,sp = evaluate_mlp(h,test)
print_evaluation(cm,a,p,r,sn,sp,h,eta,epochs)
