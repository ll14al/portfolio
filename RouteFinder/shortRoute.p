	# Gnuplot script file for plotting data in file "shortRoute.dat"
	# This file is called shortRoute.p
	unset log                              # remove any log-scaling
	unset label                            # remove any previous labels
	set xtic auto                          # set xtics automatically
	set ytic auto                          # set ytics automatically
	set title "Data Set from final_map.jpeg shown with shortest route"
	set xlabel "Latitude "
	set ylabel "Longitude "
	set key on
	unset log                              # remove any log-scaling
	unset label                            # remove any previous labels
	#set label "Yield Point" at 0.003,260
	#set xr [0.0:0.022]
	#set yr [0:325]
	plot    "shortRoute.dat" using 1:2 title 'full data set' with linespoints , \
		"shortRouteGreen.dat" using 1:3 title 'shortest route' with linespoints
