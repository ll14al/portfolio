#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "header.h"

/*
 *
 *      memgmt.c
 *
 * This module contains functions for freeing allocated memory is use
 * with the adjacency list data structure
 *
 *       by Andrew J Lumsden,  05/05/2016
 *
 */

void freeEdges(Edge *head)
{// frees the edge list, of which the passed edge is the head off.

	Edge* tmp;

	while (head != NULL)
	{// Iterative walk down the edge list
	    tmp = head;
	    head = head->nextEdge;
	    free(tmp);
	}

}

void freeStruct(Node *head)
{// frees adjacency list nodes and edges

	Node* tmp;

	while (head != NULL)
	{// Iterative walk down the node list
	    tmp = head;
	    head = head->nextNode;
	    freeEdges(head->firstLink);
	    free(tmp);
	}
}
