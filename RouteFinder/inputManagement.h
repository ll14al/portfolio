#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

/*	 fileInput.h
 *
 * This is the header file for the inputManagement.h.
 * It contains all function prototypes and
 * structure definitions reuiqred.
 *
 *      by Andrew J Lumsden,  05/04/2016
 */



unsigned char *makeBuffer( int, char *);
int intConvert(char *a);
double doubConvert(char *a);
char *cutString(int size, int b, unsigned char *buffer);
int findNextSpace(unsigned char *buffer, int pos);
int findNodeRecords(int pos, char *);
int getIndex(int a, unsigned char *buffer);
double getLatitude(int a, unsigned char *buffer);
double getLongitude(int a, unsigned char *buffer);
int get1stNodeId(int a, unsigned char *buffer);
int get2ndNodeId(int a, unsigned char *buffer);
double getLength(int a, unsigned char *buffer);
