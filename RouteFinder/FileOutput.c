#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "header.h"


/*
 *
 *      FileOutput.c
 *
 * This module contains functions for outputting the struct
 * to a file for viewing in gnu-plot.
 *
 *       by Andrew J Lumsden,  05/05/2016
 *
 */

void plotEdges( FILE *dat,FILE *srdat, Node *node, AdjList *list )
{// writes relevant data from node into file
 // for viewing in GNU plot

	//CHANGE

    // node data
    double lat = node->Latitude;
    double lon = node->Longitude;
    bool n1 =node->inReqRoute;


    Edge* walk= node->firstLink;

    	while(walk!=NULL)
    	{// walk down edge list

    		Node *dest = findNode(walk->destinationIndex,list);
    		bool n2 =dest->inReqRoute;

    		double destlon = dest->Longitude;
    		double destlat = dest->Latitude;

    		if(n1&&n2)
    		{//edge is highlighted as in shortest route.
    			fprintf(srdat, "%g 0 %g\n",lon,lat);
    			fprintf(srdat, "%g 0 %g\n\n",destlon,destlat);

    		}
    		else
    		{
    			fprintf(dat, "%g %g\n",lon,lat);
    			fprintf(dat, "%g %g\n\n",destlon,destlat);

    		}


    		walk=walk->nextEdge;
    	}

    return;
}


void plotOutput( Node *head, AdjList *list )
{// Obtains leafs from linked list structure, calls printOut()
 // on them.



	FILE *dat = fopen("shortRoute.dat","w");
	FILE *srdat = fopen("shortRouteGreen.dat","w");
	FILE *srs = fopen("shortRoute.p","w");

	//print contents of script
	fprintf(srs, "	# Gnuplot script file for plotting data in file \"shortRoute.dat\"\n");
	fprintf(srs, "	# This file is called shortRoute.p\n");
	fprintf(srs, "	unset log                              # remove any log-scaling\n");
	fprintf(srs, "	unset label                            # remove any previous labels\n");
	fprintf(srs, "	set xtic auto                          # set xtics automatically\n");
	fprintf(srs, "	set ytic auto                          # set ytics automatically\n");
	fprintf(srs, "	set title \"Data Set from final_map.jpeg shown with shortest route\"\n");
	fprintf(srs, "	set xlabel \"Latitude \"\n");
	fprintf(srs, "	set ylabel \"Longitude \"\n");
	fprintf(srs, "	set key on\n");
	fprintf(srs, "	unset log                              # remove any log-scaling\n");
	fprintf(srs, "	unset label                            # remove any previous labels\n");
	fprintf(srs, "	#set label \"Yield Point\" at 0.003,260\n");
	fprintf(srs, "	#set xr [0.0:0.022]\n");
	fprintf(srs, "	#set yr [0:325]\n");
	fprintf(srs, "	plot    \"shortRoute.dat\" using 1:2 title 'full data set' with linespoints , \\\n");
	fprintf(srs, "		\"shortRouteGreen.dat\" using 1:3 title 'shortest route' with linespoints\n");

	fclose(srs);

	//print header of .dat file
	fprintf(dat, "# This file is called   shortestroute.dat\n");
	fprintf(dat, "# Shortest route output for program\n");
	fprintf(dat, "# latitude    longitude\n\n");


	//print header of .dat file
	fprintf(srdat, "# This file is called   shortestroute.dat\n");
	fprintf(srdat, "# Shortest route output for program\n");
	fprintf(srdat, "# contains plot for shortest route\n");
	fprintf(srdat, "# latitude    longitude\n\n");


	while(head!= NULL)
	{

		int i = countEdges(head);

		if(i!=0)
		{//if node has edges
			plotEdges(dat,srdat,head,list);

		}
		head = head->nextNode;
	}

	fclose(dat);
	fclose(srdat);


	return;
}


void flagNodesSR(AdjList *list, int id)
{// flags node, to indicate it is in the requested route

	Node *head = findNode(id,list);
	//printf("index in route = %d",head->index);

	while(head!= NULL)
	{// walk through shortest route linked list

		head->inReqRoute=true;
		head = head->shortestRoute;
	}

}
