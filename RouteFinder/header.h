#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdbool.h>

/*	 header.h
 *
 * This is the header file for all modules
 * of this program. It contains all function
 * prototypes and structure definitions.
 *
 *      by Andrew J Lumsden,  05/04/2016
 */

typedef struct adjacencyListEdge
{// definition of links used in adjacencyList
	int destinationIndex;
	double distance;
	struct adjacencyListEdge *nextEdge;

} Edge;

typedef struct adjacencyListNode
{// definition of node used in adjacencyList
	int index;
	double Longitude;
	double Latitude;
	int incomingNodeSR;
	double distanceSR;
	struct adjacencyListNode *shortestRoute;
	struct adjacencyListNode *nextNode;
	struct adjacencyListEdge *firstLink;
	bool inReqRoute;
	bool isOrigin;
	bool visited;

} Node;

typedef struct adjacencyList
{// definition of node used in adjacencyList
	struct adjacencyListNode *headNode;

} AdjList;

void freeEdges(Edge *);
void freeStruct(Node *);
void addEdges(AdjList *, unsigned char *);
void add2NodeList(AdjList *, Node *);
Node *searchNodeList( int, AdjList *);
void makeLink( int , int , double , AdjList *);
Edge *makeEdge(int, double);
Node *makeNode( int , double, double);
AdjList *initAdjList();
void makeNodeList(AdjList *, unsigned char *);
Node *findNode(int , AdjList *);
void addEdgeToNode(int , Edge *, AdjList *);
void dijktrasAlgo(Node *, AdjList *, Node*, Node*, Edge*);
int countEdges(Node *);
void swapEdges(Edge *, Edge *);
void sortEdges(Node *);
void printEdgestc(Node *);
void printNodestc(Node *);
void plotEdges( FILE *,FILE *, Node *, AdjList *);
void plotOutput( Node * ,AdjList * );
void flagNodesSR(AdjList *, int);
void countNodes(Node *);
void printNodetc(Node *node);
void countTotalEdges(Node *head);
void testDAeffect(Node *);
void checkPath(AdjList *list, int id);
void frontForty(unsigned char *buffer);
void backForty(unsigned char *buffer);

unsigned char *makeBuffer( int, char *);
int intConvert(char *a);
double doubConvert(char *a);
char *cutString(int size, int b, unsigned char *buffer);
int findNextSpace(unsigned char *buffer, int pos);
int findNodeRecords(int pos, char *);
int getIndex(int a, unsigned char *buffer);
double getLatitude(int a, unsigned char *buffer);
double getLongitude(int a, unsigned char *buffer);
int get1stNodeId(int a, unsigned char *buffer);
int get2ndNodeId(int a, unsigned char *buffer);
double getLength(int a, unsigned char *buffer);
