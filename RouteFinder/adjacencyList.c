#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "header.h"




/*
 *
 *      adjacencyList.c
 *
 * This module contains functions for creating the
 * adjacency list data structure.
 *
 *       by Andrew J Lumsden,  05/05/2016
 *
 *
 */

AdjList *initAdjList()
{// initialises the pointer to the list, represents overall structure

	AdjList *list = (AdjList *)malloc(sizeof(AdjList));
	list->headNode=NULL;
	return list;
}

void makeNodeList(AdjList *list, unsigned char *buffer)
{// creates a node list by searching through buffer for records, using
 // inputManagement.c functions to fetch the data, creating nodes from the
 // data and adding the nodes to the end of the list.

		Node *node=list->headNode;

		int size = (strlen(buffer)); int i;
	    char *nodeRecStart = "<node\0";


	    for(i=0;i<size-10;i++)
	    {// searches for beginning of records "<node\0"

	        char stringForComp[6];
	        stringForComp[0] = buffer[i];
	        stringForComp[1] = buffer[i+1];
	        stringForComp[2] = buffer[i+2];
	        stringForComp[3] = buffer[i+3];
	        stringForComp[4] = buffer[i+4];
	        stringForComp[5] = '\0';

	        if(strcmp(stringForComp,nodeRecStart)==0)
	        {// gets data from records when beginning of record is found
	        	int index = getIndex(i,buffer);
	        	double longitude = getLongitude(i,buffer);
	        	double latitude = getLatitude(i,buffer);
	        	node = makeNode(index,longitude,latitude);
	        	add2NodeList(list,node);
	        }

	    }
}

Node *makeNode( int index, double longitude, double latitude )
{// creates and returns a node, setting attributes from the
 // supplied parameters.

    // allocate the data structure
    Node *node = (Node *)malloc(sizeof(Node));

    node->index = index;
    node->Longitude = longitude;
    node->Latitude = latitude;
    node->distanceSR = 0;
    node->shortestRoute = NULL;
    node->firstLink = NULL;
    node->nextNode = NULL;
    node->inReqRoute = false;
    node->isOrigin= false;
    node->visited = false;

    return node;
}

void add2NodeList(AdjList *list, Node *node)
{// adds passed node to end of list

	Node *head = &list->headNode;

    if(head == NULL)
    {// if head is empty add to head
        list->headNode= &node;
        return;
    }
    else
    {// add to end of list

        while(head->nextNode!=NULL)
        {//walk through list till end
        	head=head->nextNode;
        }
        head->nextNode = node;
        //add leaf to end
    }

    return;
}

void addEdges(AdjList *list, unsigned char *buffer)
{// adds passed edge to edge list on relevant node


	int i =0; int f=0;
	int size = (strlen(buffer));
	char *nodeRecStart = "<link\0";

	for(i=0;i<size-10;i++)
	{// for characters in buffer

		char stringForComp[6];
	    stringForComp[0] = buffer[i];
	    stringForComp[1] = buffer[i+1];
	    stringForComp[2] = buffer[i+2];
	    stringForComp[3] = buffer[i+3];
	    stringForComp[4] = buffer[i+4];
	    stringForComp[5] = '\0';

	    if(strcmp(stringForComp,nodeRecStart)==0)
	    {// beginning of edge record

	      	int i1 = get1stNodeId(i,buffer);
	       	int i2 = get2ndNodeId(i,buffer);
	       	double len = getLength(i,buffer);
	       	makeLink(i1,i2 ,len, list);

	    }
	}
}



void makeLink(int index1, int index2 , double length, AdjList *list )
{// creates a link between two nodes by setting a uni directional edge
 // both ways.


	Edge * edge = makeEdge(index2, length);
    addEdgeToNode(index1,edge,list);

    edge = makeEdge(index1, length);
    addEdgeToNode(index2,edge,list);

}

Edge * makeEdge(int destIndex, double distance )
{// makes edge from supplied data and return a pointer to the edge.

	Edge *edge = (Edge *)malloc(sizeof(Edge));
	edge->destinationIndex = destIndex;
	edge->distance = distance;
	edge->nextEdge = NULL;
	return edge;
}

void addEdgeToNode(int id, Edge *edge, AdjList *list)
{// find relevant node for edge and add it to the nodes edge list


	Node *node = findNode(id, list); //search for node to add edge to

	if(node->firstLink==NULL)
	{//edge list empty
		node->firstLink=edge;
	}
	else
	{// add to end of list
		Edge *walkEdge = node->firstLink;

		while(walkEdge->nextEdge!=NULL)
		{
			walkEdge=walkEdge->nextEdge;
		}
		walkEdge->nextEdge = edge;
	}
}


Node *findNode(int id, AdjList *list)
{// returns node with pass id

	Node *head = &list->headNode;

	while(head!=NULL)
	{// walk through node list
		if(head->index==id)
		{
			return head;
		}
		head=head->nextNode;
	}
	return head;
}




void sortEdges(Node *node)
{// sorts edges on edge list into order based on their distance, from
 // shortest to longest. This is so Dijktra's algorithm can be called on
 // nodes these edges point to, closest first.
 // -Uses bubble sort algorithm.

	Edge *next; Edge *walk;


	while(node!=NULL)
	{// walk down node list


		int c = countEdges(node);

		if(c!=0 && c!=1)
		{// if edges on node

			walk = node->firstLink;

			int i;
			for(i=0;i<c-1;i++)
			{// bubble sort algorithm requires n-1 runs through data
			 // for n members of data set

				while(walk->nextEdge!=NULL)
				{// walk through edge in edge list

					next=walk->nextEdge;
					double d1 = walk->distance;
					double d2 = next->distance;

					if(d1>d2)
					{// distance of current edge is greater than
					 // that of the next edge, swap.
						swapEdges(walk,next);
					}

					walk=walk->nextEdge;
				}
				walk=node->firstLink;// resets walkegde
			}
		}
		node=node->nextNode;
	}
}


int countEdges(Node *node)
{// is passed a node and counts how many edges on its edge list

	Edge *edge = node->firstLink;

	int i=0; // used to store edge count

	while(edge!=NULL)
	{// walk through edges
		i++;
		edge=edge->nextEdge;
	}
	return i;
}

void swapEdges(Edge *first,Edge *scnd)
{// swaps the contents of two edges

	int tIndex = first->destinationIndex;
	first->destinationIndex = scnd->destinationIndex;
	scnd->destinationIndex = tIndex;

	double tDist = first->distance;
	first->distance = scnd->distance;
	scnd->distance = tDist;

}


