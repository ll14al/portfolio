#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "header.h"


/*
 *
 *      testingf.c
 *
 * This module contains functions usefull for testing.
 *
 *       by Andrew J Lumsden,  05/05/2016
 *
 */

void printEdgestc(Node *node)
{// print edges of given node

	Edge* walk= node->firstLink;

	while(walk!=NULL)
	{// walk down edge list
		printf("\n\nindex1 = %d\n",node->index);
		printf("index2=%d\ndistance=%g\n\n",walk->destinationIndex,walk->distance);
		walk=walk->nextEdge;
	}
}

void printNodestc(Node *node)
{// prints index of nodes in node list

	//printf("index=%d\n\n",node->index);

	while(node!=NULL)
	{// walk down node list
		printf("index=%d\n\n",node->index);
		node=node->nextNode;
	}

}

void testDAeffect(Node *node)
{// counts nodes effected by dijkstra's algorithm

	int i=0;
	while(node!=NULL)
	{// walk down node list;

		if(node->shortestRoute!=NULL)
		{
			//printf("id=%d\n",node->index);
			i++;
		}
		node=node->nextNode;
	}
	//printf("%d",i);
}

void countNodes(Node *node)
{// Function for counting the amount of nodes
 // in the adjacency list.


	int count=0;

	while(node!=NULL)
	{// walk down node list

		count++;
		node=node->nextNode;
	}
	printf("\n\n\n number of nodes in list=%d",count);

}

void printNodetc(Node *node)
{// prints main contents of node

	int a = node->index;
	double b = node->Latitude;
	double c = node->Longitude;
	printf("\n\nindex=%d\nLatitude=%g\nLongitude=%g\n",a,b,c);

}

void countTotalEdges(Node *head)
{// counts total number of edges in file

	int total=0;

	while(head!=NULL)
	{// walk down node list

		total=total+countEdges(head);
		head=head->nextNode;
	}
	printf("\n\ntotal edges = %d",total);
}

void checkPath(AdjList *list, int id)
{// prints path and total distance covered

	Node *head = findNode(id,list);
	double totalD = head->distanceSR;
	printf("\n\ntotal distance = %g\n",totalD);
	while(head!= NULL)
	{// walk through shortest route linked list

		printf("index = %d\n",head->index);
		head->inReqRoute=true;
		head = head->shortestRoute;
	}

}

void frontForty(unsigned char *buffer)
{//Prints first 40 spaces in buffer

	int i = 0;
	for(i=0;i<40;i++)
	{

		printf("%c",buffer[i]);
	}

}

void backForty(unsigned char *buffer)
{//Prints back 40 spaces in buffer

	int i = 0;
	for(i=0;i<40;i++)
	{

		printf("%c",buffer[630060-41+i]);
	}

}
