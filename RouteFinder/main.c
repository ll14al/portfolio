#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "header.h"




int main( int argc, char **argv )
{//
	int nIDone = intConvert(argv[1]);
	int nIDtwo = intConvert(argv[2]);

	unsigned char *buffer = makeBuffer(argc, argv[3]);
	AdjList *list = initAdjList();

	Node *origin = (Node*)malloc(sizeof(Node));
	origin= findNode(nIDone,list);
	Node *dest = (Node*)malloc(sizeof(Node));
	dest = findNode(nIDtwo,list);

	// create sorted data structure
	makeNodeList(list, buffer);
	addEdges(list,buffer);

	int id = -8847;
	origin = findNode(id,list);
	sortEdges(origin);

	//starting algorithm
	Node *a=NULL; Node *b=NULL; Edge *w=NULL;
	id =-8847;
	origin = findNode(nIDone,list);
	origin->isOrigin=true;
	dijktrasAlgo(origin,list,a,b,w);
	origin->shortestRoute=NULL;

	flagNodesSR(list, nIDtwo);
	plotOutput(origin,list);// plot gnu plot

	free(origin);
	freeStruct(list->headNode);
	free(list);

	return 0;
}

