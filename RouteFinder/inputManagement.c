#include "header.h"
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>


/*
 *
 *      inputManagement.c
 *
 * This module contains function for reading the input file
 * and fetching relelvent data from it.
 *
 *       by Andrew J Lumsden,  05/05/2016
 *
 */


unsigned char *makeBuffer(int argc, char *filename)
{// transfers file contents to a string

	unsigned char *buffer;

	//OPENING FILE
	FILE *in_file;
	in_file = fopen(filename, "r");

	if(in_file==NULL)
	{//Error if file not present
		printf("cannot open file, file not present\n");
		exit(1);
	}

	//AQUIRING LENGTH OF FILE
	fseek(in_file,0,SEEK_END); //find end
	int size = ftell(in_file); //get position
	fseek(in_file,0, SEEK_SET); ////move back to start

	//ALLOCATING MEM FOR BUFFER
	buffer = (unsigned char*) malloc(size+1);
	buffer[size]= '\0';

	//COPY FILE INTO BUFFER
	fread(buffer, size, 1, in_file);

	fclose(in_file);

	return buffer;
}

int intConvert(char *a)
{// converts passed string to integer

    char *end;
    double i;
    i = strtod(a, &end);
    return i;
}

double doubConvert(char *a)
{// converts passed string to a double

    char *end;
    double i = 99999999;
    i = strtod(a, &end);

    return i;
}

char *cutString(int size, int b, unsigned char *buffer)
{// returns string beginning at buffer[b] of length size
    int i;
    int x = sizeof(char);
    char *idString = (char*)malloc ((x * size)+1);
    for(i=0;i<=size;i++)
    {// creates string
        idString[i] = buffer[b+i];
    }
    idString[size+1] = '\0';

    return idString;
}

int findNextSpace(unsigned char *a, int pos)
{// find the next space in buffer after a
 // passed position

    int i = pos;
    int size = (strlen(a));


    for(i=pos;i<size-1;i++)
    {
        int firstChar = a[pos];
        if( i==pos && firstChar==32)
        {
            continue;
        }
        int b = 32;
        int c = a[i];
        if(b==c)
        {
           return i;
        }

    }
    return 0;
}



int getIndex(int a, unsigned char *buffer)
{// returns the index of a node record from buffer

    int b = a+9;
    int size = findNextSpace(buffer,b)-b;
    //char *indexString= cutString(size,b,buffer);

    int i;
    char idString[size+1];
    for(i=0;i<=size;i++)
    {// creates string
        idString[i] = buffer[b+i];
    }
    idString[size+1] = '\0';


    int index = intConvert(idString);
    //free(indexString);
    return index;
}

double getLatitude(int a, unsigned char *buffer)
{// returns the latitude of a node record from buffer

    int b = a+9;
    int c = findNextSpace(buffer,b);
    int d = findNextSpace(buffer,c);
    int pos=c+5;
    int size = d-(c+5);


    int i;
    char idString[size+1];
    for(i=0;i<=size;i++)
    {// creates string
       idString[i] = buffer[pos+i];
    }
    idString[size+1] = '\0';


    double latitude = doubConvert(idString);
    return latitude;
}

double getLongitude(int a, unsigned char *buffer)
{// returns the Longitude of a node record from buffer


    int b = a+9;
    int c = findNextSpace(buffer,b);
    int d = findNextSpace(buffer,c);
    int e = findNextSpace(buffer,d);
    int pos=d+5;
    int size = e-(d+5);

    int i;
    char idString[size+1];
    for(i=0;i<=size;i++)
    {// creates string
       idString[i] = buffer[pos+i];
    }
    idString[size+1] = '\0';

    double longitude = doubConvert(idString);
    return longitude;
}

int get1stNodeId(int a, unsigned char *buffer)
{// returns the index of first node involved in the link

    int x = a+9;
    int b = findNextSpace(buffer,x);
    int c = findNextSpace(buffer,b);
    int size = c-b-1-5;
    int pos = b+6;

    int i;
    char idString[size+1];
    for(i=0;i<=size;i++)
    {// creates string
        idString[i] = buffer[pos+i];
    }
    idString[size+1] = '\0';



    int nodeIdi = intConvert(idString);
    return nodeIdi;
}

int get2ndNodeId(int a, unsigned char *buffer)
{// returns the index of the second node involved in the link

    int x = a+9;
    int b = findNextSpace(buffer,x);
    int c = findNextSpace(buffer,b);
    int d = findNextSpace(buffer,c);
    int size = d-c-1-5;
    int pos = c+6;

    int i;
    char idString[size+1];
    for(i=0;i<=size;i++)
    {// creates string

    	idString[i] = buffer[pos+i];
    }
    idString[size+1] = '\0';

    double nodeIdi = doubConvert(idString);
    return nodeIdi;
}

double getLength(int a, unsigned char *buffer)
{// returns the lenght record of an edge

    int x = a+9;
    int b = findNextSpace(buffer,x);
    int c = findNextSpace(buffer,b);
    int d = findNextSpace(buffer,c);
    int e = findNextSpace(buffer,d);
    int f = findNextSpace(buffer,e);
    int size = f-e-1-5;
    int pos = e+8;

    int i;
    char idString[size+1];

    for(i=0;i<=size;i++)
    {// creates string

    	idString[i] = buffer[pos+i];
    }
    idString[size+1] = '\0';

    double lenght = doubConvert(idString);
    return lenght;
}
