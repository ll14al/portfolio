This program produces the shortest path between two nodes in a data set 
representing the roads of central leeds. The shortest path is output in the form
of a map with the shortest route highlighted in green.


command layout for running the program 
./ RouteFinder node1 node2 inputfile

to view ouput use the following command
linux command: gnuplot -p shortRoute.p 
