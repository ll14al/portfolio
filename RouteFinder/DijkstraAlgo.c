#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "header.h"




void dijktrasAlgo( Node * origin, AdjList *list, Node* next, Node* dest, Edge *walkEdge )
{// This function is responsible for calculating the shortest path to a source node
 // from every other node in the data set. For more information on how this algorithm
 // works see included design document.
 // - externally initialised pointers used to reduce mem allocation as this is recursive function

	walkEdge = origin->firstLink; // edge to go through edge list

	while(walkEdge!=NULL)
	{//for every edge out of origin

		dest = findNode(walkEdge->destinationIndex,list);
		double a = origin->distanceSR+walkEdge->distance; // distance current route will take
		double b = dest->distanceSR;// current distance of shortest route to dest

		if(b==0 || a<b)
		{// if shorter route to node, change
			dest->distanceSR=a;
			dest->incomingNodeSR=origin->index;
			dest->shortestRoute= origin;
		}

		walkEdge=walkEdge->nextEdge; // walk down list

	}
	walkEdge = origin->firstLink; // reset walkEdge

	origin->visited=true; // make sure this node isn't visited again.

	while(walkEdge!=NULL)
	{// walk down ordered edge list (shortest first)

		//find node edge points to
		next = findNode(walkEdge->destinationIndex,list);

		if(next->visited!=true)
		{// if node hasn't already been assessed

			dijktrasAlgo( next,list, next, dest, walkEdge);
		}
		walkEdge= walkEdge->nextEdge;
	}

}
