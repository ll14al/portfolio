`timescale 1 ns/100 ps

module MultiChannelRow_tb;

// Parameter Declarations
localparam NUM_CYCLES = 20; //Simulate this many clock cycles. Max. 1 billion
localparam CLOCK_FREQ = 50000000; //Clock frequency (in Hz)
localparam RST_CYCLES = 2; //Number of cycles of reset at beginning.

// DUT parameter delcarations 
localparam FILTER_WIDTH = 3;
localparam EXT_CONSTANT = 128;
localparam NUM_CHANNELS = 2;

// Test Bench Generated Signals
reg 				   clock;
reg 			       clken;
reg 			       reset;
reg [FILTER_WIDTH-1:0] row_border;
reg       			   col_border;
reg [(8*NUM_CHANNELS)-1:0] in;

//DUT output singal
wire [(8*NUM_CHANNELS)-1:0] row_out;

//Devices Under Test - DUT
MultiChannelRow #(
	.WIDTH			(FILTER_WIDTH),
	.NUM_CHANNELS   (NUM_CHANNELS),
	.EXT_CONSTANT	(EXT_CONSTANT)

) dut (
   .clock    			( clock      ),
   .row_handling_mux_sel( row_border ),
   .col_handling_mux_sel( col_border ),
   .signal   			( in         ),
   .filtered_pixel_row 	( row_out    )
   
);


// Reset Logic
initial begin
	 reset = 1'b1; //Start in reset.
	 repeat(RST_CYCLES) @(posedge clock); //Wait for a couple of clocks
	 reset = 1'b0; //Then clear the reset signal.
end


//Clock generator + simulation time limit.
initial begin
	clock = 1'b0; //Initialise the clock to zero.
end

//Next we convert our clock period to nanoseconds and half it
//to work out how long we must delay for each half clock cycle
//Note how we convert the integer CLOCK_FREQ parameter it a real
real HALF_CLOCK_PERIOD = (1000000000.0 / $itor(CLOCK_FREQ)) / 2.0;

//Now generate the clock
integer half_cycles = 0;

always begin
 //Generate the next half cycle of clock
	 #(HALF_CLOCK_PERIOD); //Delay for half a clock period.
	 clock = ~clock; //Toggle the clock
	 half_cycles = half_cycles + 1; //Increment the counter
	
	//Print to console that the simulation has started. $time is the current simulation time.
    $display("%d ns\tSimulation Started",$time);  
    //Monitor any changes to any values listed. 
    
	
	 //Check if we have simulated enough half clock cycles
	 if (half_cycles == (2*NUM_CYCLES)) begin
		 //Once the number of cycles has been reached
		 $stop; //Break the simulation
		 //Note: We can continue the simualation after this breakpoint using
		 //"run -continue" or "run ### ns" in modelsim.
	 end
end



initial begin 
	in = 16'b1000000001000000;
	col_border = 1'b0;
	row_border = 7'd0;
end 


integer count = 1;
always @(posedge clock) begin
	
	
	
	// alternate input to ensure ensure corect updates to registers. 
	/*
	if (count == 1 ) begin
		in = 8'd250;
		count = 0;
		
	end else if (count == 0 ) begin
		in = 8'd150;
		count = count + 1;		
	end
	*/
	
	
	
end

always @(negedge clock) begin
	
	 
end

endmodule