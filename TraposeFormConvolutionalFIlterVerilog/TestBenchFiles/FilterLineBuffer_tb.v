/*
 * Freq Divider Test Bench
 * ===========================
 *
 * Test bench to test the functionality of the frequency divider's
 * ability to produce a signal as close to 128KHz as possible.
 *
 * Andrew J Lumsden 
 *(adapted from code provided in lab)
 *
 */
 
  // synopsys translate_off
`timescale 1 ps / 1 ps
 
module FilterLineBuffer_tb;

//
// Parameter Declarations
//
localparam NUM_CYCLES  = 640*4*2;       //Simulate this many clock cycles. Max. 1 billion
localparam iCLOCK_FREQ = 25000000;    //Clock frequency (in Hz)
localparam RST_CYCLES  = 2;           //Number of cycles of reset at beginning.

//
// Test Bench Generated Signals
// 
//input
reg clock;
reg mCCD_LVAL;
reg read_req;

reg [7:0]iR;
reg [7:0]iG;
reg [7:0]iB;
reg [9:0]wraddress;
reg [9:0]rdaddress;


//output
wire [23:0]oRGB0;
//wire [23:0]oRGB1;
//wire [23:0]oRGB2;

//
// Device Under Test
//
FilterLineBuffer d1(
	.PXLCLK       (clock),
	.VGACLK       (clock),
	.mCCD_LVAL    (mCCD_LVAL),
	.READ_Request (read_req), 
	.iR           (iR),
	.iG           (iG),
	.iB           (iB),
	.rdaddress    (rdaddress),
	.wraddress    (wraddress),
	
	//.oRGB0      (oRGB0),	
	//.oRGB1      (oRGB1),
	.oRGB0        (oRGB0)
);

//
// Reset Logic
//


//
//Clock generator + simulation time limit.
//
initial begin
    clock = 1'b0; //Initialise the clock to zero.
end

//Next we convert our clock period to nanoseconds and half it
//to work out how long we must delay for each half clock cycle
//Note how we convert the integer CLOCK_FREQ parameter it a real
real HALF_CLOCK_PERIOD = (1000000000000.0 / $itor(iCLOCK_FREQ)) / 2.0; 

//Now generate the clock
integer half_cycles = 0;
always begin
    //Generate the next half cycle of clock
    #(HALF_CLOCK_PERIOD);          //Delay for half a clock period.
    clock = ~clock;                //Toggle the clock
    half_cycles = half_cycles + 1; //Increment the counter
    
    //Check if we have simulated enough half clock cycles
    if (half_cycles == (2*NUM_CYCLES)) begin 
        //Once the number of cycles has been reached
		half_cycles = 0; 		   //Reset half cycles, so if we resume running with "run -all", we perform another chunk.
        $stop;                     //Break the simulation
        //Note: We can continue the simulation after this breakpoint using 
        //"run -continue" or "run ### ns" in modelsim.
    end
end

initial begin 
	wraddress = 0;
	rdaddress = 0;
end 
integer count = 0; 

always @( posedge clock) begin 
	
	read_req = 1'b1;
	iR = wraddress; // writing write address to ram
	iG = wraddress; // writing write address to ram
	iB = wraddress; // writing write address to ram	
	
end 

always @( negedge clock ) begin // inc variables on negedge 
	
	rdaddress = rdaddress + 1;
	wraddress = wraddress + 1;
	count = count + 1;
	
	if( count == 640 )begin // provide a pulse after each line has been read. 
		mCCD_LVAL = 0;
		count = 0;
	end else begin 
		mCCD_LVAL = 1;
	end 
	
	if(rdaddress == 640) begin rdaddress = 0; end 
	if(wraddress == 640) begin wraddress = 0; end 

end 

endmodule

