/*
 * Freq Divider Test Bench
 * ===========================
 *
 * Test bench to test the functionality of the frequency divider's
 * ability to produce a signal as close to 128KHz as possible.
 *
 * Andrew J Lumsden 
 *(adapted from code provided in lab)
 *
 */
 
  // synopsys translate_off
`timescale 1 ps / 1 ps
 
module RowBuffers_tb;

//
// Parameter Declarations
//
localparam NUM_CYCLES  = 640*4*2;       //Simulate this many clock cycles. Max. 1 billion
localparam iCLOCK_FREQ = 25000000;    //Clock frequency (in Hz)
localparam RST_CYCLES  = 2;           //Number of cycles of reset at beginning.

// DUT parameter delcarations 
localparam FILTER_WIDTH = 5;
localparam NUM_CHANNELS = 2;

//
// Test Bench Generated Signals
// 
//input
reg clock;
reg clken;
reg [(8*NUM_CHANNELS)-1:0]shiftin;

//output
wire [(8*NUM_CHANNELS)-1:0]shiftout;


// Device Under Test
wire [(8*NUM_CHANNELS*(FILTER_WIDTH-2))-1:0]link; // test connector
RowBuffers #(
	.WIDTH(FILTER_WIDTH),
	.NUM_CHANNELS(NUM_CHANNELS)
)d1(
	
	.clken     (clken),
	.clock     (clock),
	.shiftin   ({link,shiftin}),
	.shiftout  ({shiftout,link})
);

//
// Reset Logic
//


//
//Clock generator + simulation time limit.
//
initial begin
    clock = 1'b0; //Initialise the clock to zero.
end

//Next we convert our clock period to nanoseconds and half it
//to work out how long we must delay for each half clock cycle
//Note how we convert the integer CLOCK_FREQ parameter it a real
real HALF_CLOCK_PERIOD = (1000000000000.0 / $itor(iCLOCK_FREQ)) / 2.0; 

//Now generate the clock
integer half_cycles = 0;
always begin
    //Generate the next half cycle of clock
    #(HALF_CLOCK_PERIOD);          //Delay for half a clock period.
    clock = ~clock;                //Toggle the clock
    half_cycles = half_cycles + 1; //Increment the counter
    
    //Check if we have simulated enough half clock cycles
    if (half_cycles == (2*NUM_CYCLES)) begin 
        //Once the number of cycles has been reached
		half_cycles = 0; 		   //Reset half cycles, so if we resume running with "run -all", we perform another chunk.
        $stop;                     //Break the simulation
        //Note: We can continue the simulation after this breakpoint using 
        //"run -continue" or "run ### ns" in modelsim.
    end
end

initial begin 
	shiftin = 0;
	clken    = 1;
end 
reg  [7:0] count    = 0;
reg [15:0] count640 = 0;
 

always @( posedge clock) begin 
	
	shiftin[7:0]   = count;
	shiftin[15:8]  = count + 1;
	//shiftin[23:16] = count + 2;
	count640 = count640 +1;
	
end 

always @( negedge clock ) begin // inc variables on negedge 
	
	count = count + 1;
	if ( count640 == 640*3 ) begin 
		clken = 1;
	end
	if ( count640 == 640*3 +130 ) begin 
		clken = 1;
	end
	if ( count == 10 ) begin 
		count = 0;
	end
	
	
end 

endmodule

