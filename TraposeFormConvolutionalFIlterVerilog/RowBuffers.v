/*** Parameterised Row Buffer Design for Transpose From Filter ***
    
    This module instantiates multiple multi-channel 
    row buffers. Each buffer open for input and acquisition
    of output. 
    
    buffer size 640 ( VGA row size )
    
    ** This module makes use of the Alter RAM based shift 
       register design for a single channel row buffer. ** 
    
    Number of channels can be selected through 
    the parameter NUM CHANNELS. 

    
    Width of the filter the row buffer will be used for
    specified through the WIDTH parameter. 
    
    Channel width should always be 8. 
    
    clken : Enable entry of values into row buffers. 
                           
    Shiftin : Input bus that can provide input to each row 
              buffer generated.
    
    Shiftout : Output bus that allows access to the output of each
               row buffer generated. 
                        
*/


 // synopsys translate_off
`timescale 1 ps / 1 ps
// synopsys translate_on


module RowBuffers #(
	parameter WIDTH = 3,
	parameter NUM_BUFFERS = WIDTH -1,
	parameter NUM_CHANNELS = 3,
	parameter CHANNEL_WIDTH = 8,
	parameter IO_BUS_WIDTH = NUM_CHANNELS * CHANNEL_WIDTH * NUM_BUFFERS,
	parameter INTERN_BUS_WIDTH = CHANNEL_WIDTH * NUM_BUFFERS

)(

	input	        			clken,
	input	  	    			clock,
	input	[IO_BUS_WIDTH-1:0]  shiftin,
	output	[IO_BUS_WIDTH-1:0]  shiftout

);


genvar i;
generate
	
	for (i = 1; i < NUM_CHANNELS+1; i = i + 1) begin : bufferperchannel
	
		ChannelRowBuffers #(
			.WIDTH 	  (WIDTH),
			.CHANNEL_WIDTH(CHANNEL_WIDTH)
		) rb (
		
			.clock    (clock),
			.clken	 (clken),
			.shiftin	 (shiftin[(INTERN_BUS_WIDTH*i)-1:
						           INTERN_BUS_WIDTH*(i-1)] ), 
								  
			.shiftout (shiftout[(INTERN_BUS_WIDTH*i)-1:
						            INTERN_BUS_WIDTH*(i-1)] )							  
		);
		
	end	
endgenerate

endmodule



module ChannelRowBuffers #(
	parameter WIDTH = 3,
	parameter NUM_BUFFERS = WIDTH -1,
	parameter CHANNEL_WIDTH = 8,
	parameter IO_BUS_WIDTH = NUM_BUFFERS * CHANNEL_WIDTH
)(

	input	       					  clken,
	input	  	   					  clock,
	input	   [IO_BUS_WIDTH-1:0]  shiftin,
	output	[IO_BUS_WIDTH-1:0]  shiftout
	
);


genvar i ;

generate
	
	for (i = 1; i < NUM_BUFFERS+1; i = i + 1) begin : rowsinbuffer
	
		RAMShift t(
			.clken(clken),
			.clock(clock),
			.shiftin ( shiftin[((CHANNEL_WIDTH*i)-1):
						            CHANNEL_WIDTH*(i-1)] ), 
								  
			.shiftout( shiftout[((CHANNEL_WIDTH*i)-1):
						             CHANNEL_WIDTH*(i-1)] ),
			.taps()
		);
		
	end	
endgenerate

endmodule

