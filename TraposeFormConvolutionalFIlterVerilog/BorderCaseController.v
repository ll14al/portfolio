/*** Border Handling Controller for 2D Transpose Form Filter ***
	
	Border handling using constant border extension.
	
	This design is built for a filter of specifed width which can 
	be controlled through the WIDTH parameter. 
	
	row_border : Input high for a cycle when pixel at centre of 
				 filter is ROW_LENGTH - ((FILTER WIDTH+1)/2).

	col_border : Input high for a cycle when pixel at centre of 
				 filter is COL_LENGTH - ((FILTER WIDTH+1)/2).
				 
	row_handling_mux_sel: Bits used to control insertion of 
						  extension pixels into filter at row
						  edges.
						  
	col_handling_mux_sel: Bits used to control insertion of 
						  extension pixels into filter at 
						  column edges.
						
*/

module BorderCaseController #(
	// Filter width
	parameter WIDTH = 3,
	//Cetre position in array
	parameter MAX_RED_WIDTH = (WIDTH-1)>>1,
	parameter MID = MAX_RED_WIDTH
	
)( 
	input 				clock, 
	input 		 		row_border,
	input 		 		col_border,
	output [WIDTH-1:0] 	row_handling_mux_sel,
	output [WIDTH-1:0] 	col_handling_mux_sel
	
);

	reg [WIDTH-1:0] row_handling_buffer;
	reg [WIDTH-1:0] col_handling_buffer;

	// centre pixel in window M-1
	assign clken_col_control = row_handling_buffer[MAX_RED_WIDTH-1];
	
	genvar i;	
	generate 
	
		assign row_handling_mux_sel [MID] = 0; // Middle mux sel always 0
		assign col_handling_mux_sel [MID] = 0; // Middle mux sel always 0
		for (i=1; i<MAX_RED_WIDTH+1; i = i+1 ) begin : sel_logic_gen_cosntant_extension
			
			// logic to produc mux select for summand selection, border handling. 
			assign row_handling_mux_sel[i-1]     = |(row_handling_buffer[ MID-1 : (i-1)]);
			assign row_handling_mux_sel[WIDTH-i] = |(row_handling_buffer[WIDTH-1-i : MID]);
			
			// logic to produc mux select for summand selection, border handling. 
			assign col_handling_mux_sel[i-1]     = |(col_handling_buffer[MID-1 : (i-1)]);
			assign col_handling_mux_sel[WIDTH-i] = |(col_handling_buffer[WIDTH-1-i : MID]);
		end 
		
	endgenerate 
	
	initial begin 
		row_handling_buffer = 0;
		col_handling_buffer = 0;		
	end 
	
	
	always @( posedge clock ) begin
		
		// Border control buffer 
		row_handling_buffer = row_handling_buffer << 1;
		row_handling_buffer[0] = row_border;
		

	end 
	
	// Ensures columns are counted once per row 
	always @( posedge clken_col_control  ) begin
		
		col_handling_buffer = col_handling_buffer << 1;
		col_handling_buffer[0] = col_border;

	end
	
endmodule
