/*** Multi Channel Row of Tranpose form filter ***
	
	This module instantiates multiple single channel 
	filter rows to produce a multi channel row. 
	
	Number of channels can be selected through 
	the paramter NUM CHANNELS. Channel width should 
	always be 8. 
	
	row_handling_mux_sel : Select bits for multiplexers 
						   controlling the insertion of 
						   extention constants into the 
						   filter window at row borders. 
	
	col_handling_mux_sel : Select bits for multiplexers 
						   controlling the insertion of 
						   extention constants into the 
						   filter window at column borders. 
						   
	signal : Multi channel input signal.
	
	filtered_pixel_row: Output of a single row in transpose 
						form filter. 
						
*/

`define kcalc(x) ( \
 ((x) <= 3)  ? 28 : \
 ((x) <= 5)  ? 10 : \
 ((x) <= 7)  ?  5 : \
 ((x) <= 9)  ?  3 : \
 ((x) <= 11) ?  2 : 28)

module MultiChannelRow #(
	parameter NUM_CHANNELS = 3,
	parameter WIDTH = 3,
	parameter K = `kcalc(WIDTH),
	parameter CHANNEL_WIDTH = 8,
	parameter EXT_CONSTANT = 8'd128,
	parameter IO_BUS_WIDTH =  CHANNEL_WIDTH * NUM_CHANNELS

	
) ( 
	input 		 				 clock,
	input  [WIDTH-1:0] 			 row_handling_mux_sel,
	input 						 col_handling_mux_sel,
	input  [(IO_BUS_WIDTH-1):0]  signal,
	output [(IO_BUS_WIDTH-1):0]  filtered_pixel_row
	
);
	
	// Generate row for each channel of the signal 
	genvar i;
	generate
		for (i = 1; i < NUM_CHANNELS+1; i = i + 1) begin : Generate_multiple_channels
		
			TransposeFilterRow #(
				.WIDTH (WIDTH),
				.CHANNEL_WIDTH(CHANNEL_WIDTH),
				.EXT_CONSTANT(EXT_CONSTANT)
			)f(
				.clock				 (clock),
				.row_handling_mux_sel(row_handling_mux_sel),
				.col_handling_mux_sel(col_handling_mux_sel),
				.signal				 (signal[((CHANNEL_WIDTH*i)-1):
												 (CHANNEL_WIDTH*(i-1))]), 
															 
				.filtered_row		 (filtered_pixel_row[((CHANNEL_WIDTH*i)-1):
														  (CHANNEL_WIDTH*(i-1))])	
			);
			
		end	
	endgenerate

endmodule