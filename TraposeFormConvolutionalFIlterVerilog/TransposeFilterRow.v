/*** Parameterized 2D 1-Channel Convolutional Filter Row ***
	
	This module contains the implementation of a transpose 
	form filter row for a single channel. 
	
	INFO
	Width of the filter is specified by parameter. 
	Constant used for border extension can be specified 
	by paramter.
	Channel width should always be 8 
	
	Inputs
	Border handling used coefficient extension.
	
	row_handling_mux_sel : Select bits for multiplexers 
						   controlling the insertion of 
						   extention constants into the 
						   filter window at row borders. 
	
	col_handling_mux_sel : Select bits for multiplexers 
						   controlling the insertion of 
						   extention constants into the 
						   filter window at column borders. 
						   
	signal : Single channel signal to be filtered.
	
	Outputs
	filtered_row : Single channel output of a transpose 
					form filter row.
						
*/

`define kcalc(x) ( \
 ((x) <= 3)  ? 28 : \
 ((x) <= 5)  ? 10 : \
 ((x) <= 7)  ?  5 : \
 ((x) <= 9)  ?  3 : \
 ((x) <= 11) ?  2 : 28)

module TransposeFilterRow #(
	parameter WIDTH = 3,
	parameter K = `kcalc(WIDTH),
	parameter CHANNEL_WIDTH = 8,
	parameter MAX_RED_WIDTH = (WIDTH-1)>>1,
	parameter EXT_CONSTANT = 8'd128,
	parameter MID = MAX_RED_WIDTH
	
)( 
	input 		 				clock,
	input  [WIDTH-1:0] 			row_handling_mux_sel,
	input 						col_handling_mux_sel,
	input  [CHANNEL_WIDTH-1:0] 	signal,
	output [CHANNEL_WIDTH-1:0] 	filtered_row
	
);

	// Accumulation registers
	reg [(CHANNEL_WIDTH*2)-1:0] z[WIDTH-2:0]; 

	// Results of weights applied to signal or constant for extension 
	wire [(CHANNEL_WIDTH*2)-1:0] mult[1:0];

	// Used to select between multiplication results when border handling required. 	
	wire [WIDTH-1:0]sel;
	
	// Shift register to represent states in which border handling is required
	// at row transistions. 
	reg [WIDTH-1:0] row_handling_buffer;
	
	// Constant for border extension.
	reg [(CHANNEL_WIDTH*2)-1:0] constant = EXT_CONSTANT;
	
	// Reuslts of the two possible multiplications required in one cycle. 
	assign mult[0] = (signal   * K) >> 8;
	assign mult[1] = (constant * K) >> 8;

	// Final output of row
	assign filtered_row = z[WIDTH-2] + mult[sel[WIDTH-1]];
	
	// Generate sel bit for each  
	genvar i;
	generate
		for (i = 0; i < WIDTH; i = i + 1) begin : sel_bit_gen 
			assign sel[i] = row_handling_mux_sel[i] | col_handling_mux_sel;
		end	
	endgenerate
	
	
	integer loopVal; 	
	always @( posedge clock ) begin
		
		// Summand propagation through intermediate registers. 
		z[0] <=  mult[sel[0]];
		
		for (loopVal = 0; loopVal < WIDTH-2; loopVal = loopVal + 1) begin
			z[loopVal+1] <= mult[sel[loopVal+1]] + z[loopVal];
		end

	end 
	
	

endmodule