
/*** Parameterized 2D N-Channel Convolutional Filter ***
	
	INFO:
	Border handling used coefficient extension.
	Window size limit: odd sizes between 3x3 and 11x11  

	Description:
	This module generates multi channel rows of a transpose
	form filter to create a full 2D filter. Outputs of the rows
	are entered into a row buffer module for summation accross 
	columns of the filter window. The border case controller 
	module controll transition of image rows and frames. 
	
	Inputs
	
	clken : enables row buffer.
	row_border : Input high for a cycle when pixel at centre of 
				 filter is ROW_LENGTH - ((FILTER WIDTH+1)/2).

	col_border : Input high for a cycle when pixel at centre of 
				 filter is COL_LENGTH - ((FILTER WIDTH+1)/2).
				 
	signal : inptut signal to be filtered. 
	
	filtered : Filtered signal. 
						
*/

`define kcalc(x) ( \
 ((x) <= 3)  ? 28 : \
 ((x) <= 5)  ? 10 : \
 ((x) <= 7)  ?  5 : \
 ((x) <= 9)  ?  3 : \
 ((x) <= 11) ?  2 : 28)


module TransposeFormFilter #(
	parameter NUM_CHANNELS = 3,
	parameter WIDTH = 3,
	parameter K = `kcalc(WIDTH),
	parameter CHANNEL_WIDTH = 8,
	parameter EXT_CONSTANT = 8'd128, // Constant used for border extension
	parameter PIXEL_WIDTH = NUM_CHANNELS * CHANNEL_WIDTH
	
)( 
	input 		 			   clock,
	input 					   clken,
	input     				   row_border,
	input     				   col_border,
	input   [PIXEL_WIDTH-1:0]  signal, 
	output  [PIXEL_WIDTH-1:0]  filtered	
	
);
	// Stores and distributes outputs of Filter rows 
	reg  [((PIXEL_WIDTH*WIDTH)-1):0] filter_row_outs ;
	wire [((PIXEL_WIDTH*WIDTH)-1):0] filter_row_link ;
	
	// TAP into Pixel buffer at each row 
	wire [(PIXEL_WIDTH*(WIDTH-1))-1:0] buffer_row_outs_link;
	
	// Results of row accumulation. 
	wire [(PIXEL_WIDTH*(WIDTH-1))-1:0] filter_row_accum;
	
	// Final Filtered Pixel 
	assign filtered = filter_row_accum[(PIXEL_WIDTH*(WIDTH-1))-1 : 
										PIXEL_WIDTH*(WIDTH-2)];
	
	// Border case mux select busses; 
	wire [WIDTH-1:0] row_ext_sel;
	wire [WIDTH-1:0] col_ext_sel;
	
	// Instantiates multiple multi-channel rows to produce filter window.  
	genvar i ;
	generate
		for (i = 0; i < WIDTH; i = i + 1) begin : Generate_rows_in_window
		
			MultiChannelRow #(
				.NUM_CHANNELS (NUM_CHANNELS),
				.WIDTH 		  (WIDTH),
				.CHANNEL_WIDTH(CHANNEL_WIDTH),
				.EXT_CONSTANT(EXT_CONSTANT)
					
			)d(
				.clock                ( clock ),
				.row_handling_mux_sel ( row_ext_sel ),
				.col_handling_mux_sel ( col_ext_sel[i] ),
				.signal			      ( signal ), 
				
				.filtered_pixel_row( filter_row_link[(PIXEL_WIDTH*(i+1))-1:
													 (PIXEL_WIDTH*i)]       )	
			);

		end
		
		// Performs accumulations of row outputs to produce final pixel. 
		for (i = 0; i < WIDTH-1; i = i + 1) begin : Accumulate_Rows
			
			// Accumulate window row outs of Filter  
			assign filter_row_accum[(PIXEL_WIDTH*(i+1))-1:(PIXEL_WIDTH*i)] 
							= filter_row_outs[(PIXEL_WIDTH*(i+2))-1:(PIXEL_WIDTH*(i+1))] + 
							  buffer_row_outs_link[(PIXEL_WIDTH*(i+1))-1:(PIXEL_WIDTH*i)];
		end

		
	endgenerate
	

	always @( posedge clock) begin 
		filter_row_outs = filter_row_link;
	end
	
	RowBuffers #(
		.WIDTH 	  (WIDTH),
		.NUM_CHANNELS (NUM_CHANNELS),
		.CHANNEL_WIDTH(CHANNEL_WIDTH)
	) rb (
			
		.clken    (clken),
		.clock    (clock),
		.shiftin  ({filter_row_accum[(PIXEL_WIDTH*(WIDTH-2))-1:0], 
					filter_row_outs[PIXEL_WIDTH-1:0]}),
						
		.shiftout (buffer_row_outs_link)
		
	);
	
	BorderCaseController #(
		.WIDTH (WIDTH)
	)bcc( 
		.clock(clock), 
		.row_border(row_border),
		.col_border(col_border),
		.row_handling_mux_sel(row_ext_sel),
		.col_handling_mux_sel(col_ext_sel)
	
);
	
endmodule



 





