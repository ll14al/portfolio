#include "menu.h"

Menu::Menu()
{
    ;
}

/** Menu 
*
*   Contians the finite state machine logic implementation for the game menu.
*   The menu contains the option to resume game, restart game and view 
*   highscores.   
*
*/
void Menu::menuP()
{
    lcd.clear();lcd.refresh();
    bool exit=TRUE;// global varibale used for menu screen while loop.
    int sel=0;
    while(exit)
    {
        switch(sel){
        case 0: // resume
            //printf("0\n");
            sel = resumeB();wait(0.2);
            if(pad.check_event(Gamepad::A_PRESSED)==TRUE)exit=FALSE;
            break;
            
        case 1: // settings
            //printf("1\n");
            sel = settingsB();wait(0.2);
            break;
        
        case 2: // restart
            //printf("2\n");
            sel = restartB();wait(0.2);
            if(pad.check_event(Gamepad::A_PRESSED)==TRUE){
            restart=TRUE;exit=FALSE;}
            break;
        }
    }  
}

/** Resume button  
*
*   Deals with the resume button state. Returns an integer to effect the case 
*   switch statement in menu() depending on what action is applied to the analog
*   stick. If the A buton is pressed the game resumes. Fills the screen with the
*   correct view for this state indicating this buton is currently bein 
*   "hovered" over.
*
*   @return int  
*/
int Menu::resumeB()
{
    Direction dir = pad.get_direction();
    float mag= pad.get_mag();
    lcd.clear();
    lcd.refresh();
    lcd.printString(" @Resume",0,1);
    lcd.printString("  Settings",0,2);
    lcd.printString("  Restart Game",0,3);
    lcd.refresh();
    if((dir==N)&&(mag>0.7))return 2; // analogue stick up,      mag limit for 
    if((dir==S)&&(mag>0.7))return 1; // analogue stick down,    less sensitivity
    else return 0;      // stay in current state
}

/** Settings page button   
*
*   Responsible for settings button state. Prints the relevent strings to screen 
*   that indicate the settings button is being hovered over. Checks for input
*   from the ananlogue stick which is used to change the case in menu(), and
*   check the A button as if pressed the settings page will be entered. 
*   
*   @return int - used to effct case switch in menuScreen() 
*/
int Menu::settingsB()
{
    Direction dir = pad.get_direction();
    float mag = pad.get_mag();
    lcd.clear();
    lcd.refresh();
    lcd.printString("  Resume",0,1);
    lcd.printString(" @Settings",0,2);
    lcd.printString("  Restart Game",0,3);
    lcd.refresh();
    if(pad.check_event(Gamepad::A_PRESSED)==TRUE)settingsP();
    if((dir==N)&&(mag>0.7))return 0; // analogue stick up,      mag limit for 
    if((dir==S)&&(mag>0.7))return 2; // analogue stick down,    less sensitivity
    else return 1;      // stay in current state
}

/** Restart game button  
*
*   Deals with the restart game button state. Returns an integer to effect the 
*   case switch statement in menu() depending on what action is applied to the 
*   analog stick. If the A buton is pressed the game is restarted. Fills the 
*   screen with the correct view for this state indicating this buton is 
*   currently bein "hovered" over.   
*
*   @return int  
*/
int Menu::restartB()
{
    Direction dir = pad.get_direction();
    float mag= pad.get_mag();
    lcd.clear();
    lcd.refresh();
    lcd.printString("  Resume",0,1);
    lcd.printString("  Settings",0,2);
    lcd.printString(" @Restart Game",0,3);
    lcd.refresh();
    if((dir==N)&&(mag>0.7))return 1; // analogue stick up,      mag limit for
    if((dir==S)&&(mag>0.7))return 0; // analogue stick down,    less sensitivity
    else return 2;      // stay in current state
}

/** settings page  
*
*   Responsible for producing the settings page. Contains a case switch state 
*   used to produce the three interchangable states on the page. The states
*   are for toggles methods of player control on and off. The back button
*   can be pressed at anytime to return to the main menu.
*/
void Menu::settingsP()
{
    lcd.clear();lcd.refresh();
    bool exit=TRUE;
    int sel=0;
    while(exit)
    {
        switch(sel){
        case 0: // cross pad 
            //printf("0\n");
            sel = crossPadB();wait(0.2);
            if(pad.check_event(Gamepad::BACK_PRESSED)==TRUE)exit=FALSE;
            break;
            
        case 1: // ananlogue stick
            //printf("1\n");
            sel = analogStickB();wait(0.2);
            if(pad.check_event(Gamepad::BACK_PRESSED)==TRUE)exit=FALSE;
            break;
        
        case 2: // accelerometer
            //printf("2\n");
            sel = accelerometerB();wait(0.2);
            if(pad.check_event(Gamepad::BACK_PRESSED)==TRUE)exit=FALSE;
            break;
        }
    }  
}

/** cross pad toggle button
*
*   Creates the state in which toggling the scross pad controlls is possible. 
*   Prints the strings that convey the relevent information. Looks out for input
*   from the analogue stick, which determines the funtions output for switching
*   the case statement in settingsP(). If the A button is pressed a global bool
*   is effected that determins whether the cross pad controls are usable by the
*   player.
*
*   @returns int - used for affecting case switch in settingP()
*
*/
int Menu::crossPadB()
{
    Direction dir = pad.get_direction();
    float mag = pad.get_mag();
    lcd.clear();
    lcd.refresh();
    if(crossPadEnabled)lcd.printString("@cross pad on",0,1);
    else lcd.printString("@cross pad off",0,1);
    if(anlgStickEnabled) lcd.printString(" anlg stck on",0,2);
    else lcd.printString(" anlg stck off",0,2);
    if(aclrmtrEnabled) lcd.printString(" acelromtr on",0,3);
    else lcd.printString(" acelromtr off",0,3);
    lcd.refresh();
    if(pad.check_event(Gamepad::A_PRESSED)==TRUE)
        crossPadEnabled=!crossPadEnabled;
    if((dir==N)&&(mag>0.7))return 2; // analogue stick up,      mag limit for 
    if((dir==S)&&(mag>0.7))return 1; // analogue stick down,    less sensitivity
    else return 0;      // stay in current state
}

/** analogue stick toggle button
*
*   Creates the state in which toggling the ananlogue stick controls is 
*   possible. Prints the strings that convey the relevent information. Looks out
*   for input from the analogue stick, which determines the funtions output for 
*   switching the case statement in settingsP(). If the A button is pressed a 
*   global bool is effected that determins whether the analogue stick controls 
*   are usable by the player.
*
*   @returns int - used for affecting case switch in settingP()
*
*/
int Menu::analogStickB()
{
    Direction dir = pad.get_direction();
    float mag = pad.get_mag();
    lcd.clear();
    lcd.refresh();
    if(crossPadEnabled)lcd.printString(" cross pad on",0,1);
    else lcd.printString(" cross pad off",0,1);
    if(anlgStickEnabled) lcd.printString("@anlg stck on",0,2);
    else lcd.printString("@anlg stck off",0,2);
    if(aclrmtrEnabled) lcd.printString(" acelromtr on",0,3);
    else lcd.printString(" acelromtr off",0,3);
    lcd.refresh();
    if(pad.check_event(Gamepad::A_PRESSED)==TRUE)
        anlgStickEnabled=!anlgStickEnabled;
    if((dir==N)&&(mag>0.7))return 0; // analogue stick up,      mag limit for 
    if((dir==S)&&(mag>0.7))return 2; // analogue stick down,    less sensitivity
    else return 1;      // stay in current state
}

/** accelerometer toggle button
*
*   Creates the state in which toggling the acceleromter controls is possible. 
*   Prints the strings that convey the relevent information. Looks out for input
*   from the analogue stick, which determines the funtions output for switching 
*   the case statement in settingsP(). If the A button is pressed a global bool 
*   is effected that determins whether the accelerometer controls are usable by
*   the player.
*
*   @returns int - used for affecting case switch in settingP()
*
*/
int Menu::accelerometerB()
{
    Direction dir = pad.get_direction();
    float mag = pad.get_mag();
    lcd.clear();
    lcd.refresh();
    if(crossPadEnabled)lcd.printString(" cross pad on",0,1);
    else lcd.printString(" cross pad off",0,1);
    if(anlgStickEnabled) lcd.printString(" anlg stck on",0,2);
    else lcd.printString(" anlg stck off",0,2);
    if(aclrmtrEnabled) lcd.printString("@acelromtr on",0,3);
    else lcd.printString("@acelromtr off",0,3);
    lcd.refresh();
    if(pad.check_event(Gamepad::A_PRESSED)==TRUE)
        aclrmtrEnabled=!aclrmtrEnabled;
    if((dir==N)&&(mag>0.7))return 1; // analogue stick up,      mag limit for
    if((dir==S)&&(mag>0.7))return 0; // analogue stick down,    less sensitivity
    else return 2;      // stay in current state
}

