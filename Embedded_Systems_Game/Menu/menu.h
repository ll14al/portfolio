#ifndef MENU_H
#define MENU_H

#include "mbed.h"
#include "N5110.h"
#include "Gamepad.h"
#include "FXOS8700CQ.h"

#define TRUE 1
#define FALSE 0

extern N5110 lcd;
extern Gamepad pad;
extern float score;
extern bool crossPadEnabled;
extern bool anlgStickEnabled;
extern bool aclrmtrEnabled;
extern bool restart;

/** Menu Class
*
*   This class represents the game menu and contains fucntions that produce all
*   the states that make up the sub-menus and buttons. The menus and sub menus
*   provide the options to pasue and resume the game, restart the game and enter
*   the settings menu. The settings menu allows user to turn on and off the 
*   controlls they are allowed to use.  
*
*   TO DO: I wished to included a High score page which made use of the SD card 
*   to store and display high scores but i couldnt get the SD card working so 
*   this fucntionality is yet to be added. 
*  
*   @author Andrew J Lumsden
*   @date 04/05/17
*/
class Menu
{

public:
    Menu();
    ~Menu();
 
    void menuP();
    int resumeB();
    int settingsB();
    void settingsP();
    int restartB();
    int crossPadB();
    int analogStickB();
    int accelerometerB();
    
private:
    
    
};
#endif