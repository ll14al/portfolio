#ifndef ENVIROMENT_H
#define ENVIROMENT_H


#define TRUE 1
#define FALSE 0
#define PI 3.1415926535
#define ACCEL 7
#define FRICTION 0.9999
#define GRAVCONSTANT 6.6738480 * pow(10.0,-2.0)
#include "N5110.h"
#include "Gamepad.h"
#include "bubble.h"
#include "string"
#include "FXOS8700CQ.h"

extern bool deadBubble;
extern float score;
extern Gamepad pad;
extern Data values;
extern bool restart;
extern N5110 lcd;

/** Enviroment Class
*
*@brief This class represents the enviroemnt in which the game takes place. 
*@brief Its is responsible for all physics between the player and enemies, 
*@brief including collisions, mass transfer and gravity. The fucntion is also 
*@brief responsible for keeping the amount of enemies in the game correct by 
*@brief destroying a and creating where needed.
*
*   TO DO: there are a number of fucntions that are very similar to each other, thier
*   only difference being wheter they deal with enemy-enemy or player-enemy
*   interactions these could be refactored into single fucntions if the player 
*   bubble and enemies bubble array are combined into one array. 
*  
*   @author Andrew J Lumsden
*   @date 04/05/17
*/
class Enviroment
{
 
public:
    Enviroment(int n); 
    ~Enviroment();
    
    Bubble *enemies[4]; //enemy bubble array 
    Bubble *player; 
    char ncircles; // number of enemies 
 
    void update();
    void massTransfer(int b);
    void massTransfer(int a,int b);
    void instMassTransfer(int b);
    void instMassTransfer(int a,int b);
    float distPlyrBub(int i);
    float angleBtwnBubs(int);
    float angleBtwnBubs(int,int);
    float gravForceBtwnBubs(int);
    float gravForceBtwnBubs(int,int);
    float gravityAccelApply(int);
    void gravForceDirMod(int,int);
    void gravForceDirMod(int,int,int);
    void collisionDetect();
    void plyrCollideOther();
    void otherCollideOther();
    void endGameConditions();
    void dstryOffScreenBs();
    void destroyBubbles();
    void scoreUpdate();
    void rmvSmallBubbles();
    float distBtwnBubs(int);
    float distBtwnBubs(int,int);
    void gravityPlyrEnem();
    void gravityEnemEnem();
    void destroyBubble(int i);
    int checkBubbleCount();
    void bubbleMaintenance();
    void bubbleRegen();
    
    
private:
 
    
 
};

#endif