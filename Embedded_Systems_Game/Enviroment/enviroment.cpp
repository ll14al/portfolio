#include "enviroment.h"

/** destroy Off Screen Bubbles 
*
*   Cycles through enemy bubbles and remove them if they out of bounds of the
*   screen 
*/
void Enviroment::dstryOffScreenBs()
{
    // if bubble leave screen destroy    
    int i=0;
    while(enemies[i]!=NULL) {
        int x = enemies[i]->getcpx();
        int y = enemies[i]->getcpy();
        if((x<0||x>47)||(y<0||y>81)) {
            enemies[i]=NULL;
        }
        i++;
    }
}

/** destroy Bubbles 
*
*   Destroys all the enemy bubbles, used when game resets 
*
*/
void Enviroment::destroyBubbles()
{
    // sets all enemy bubbles to NULL
    int i=0;
    while(enemies[i]!=NULL) {
        enemies[i]=NULL;
        i++;
    }
}

/** destroy Bubble 
*
*   destroy bubble at the array position passed as a parameter
*
*   @param i - position in array at which the bubble should be destroyed  
*/
void Enviroment::destroyBubble(int i)
{
    // destroy bubble at passed postion in array
    enemies[i]=NULL;
}

/** Check Bubble Count 
*
*   Returns the amount of active enemy bubbles
*   
*   @return int i - amount of enemy bubbles 
*/
int Enviroment::checkBubbleCount()
{
    int i=0;
    int n=0;
    for(i=0; i<4; i++) {
        if(enemies[i]!=NULL) {
            n++;
        }
    }
    return n;
}

/** Bubble Regeneration 
*
*   if the bubble count goes below the expected amount of bubbles on screen make 
*   a new bubble
*
*/
void Enviroment::bubbleRegen()
{
    int i=0;
    while(checkBubbleCount()<ncircles) {
        while(enemies[i]!=NULL) {
            i++;
        }
        enemies[i] = new Bubble(0);
    }
}

/** remove small bubbles. 
*
*   Removes bubbles from the enviroment that are too small to be seen, as they 
*   will stop new bubbles replacing them. 
*
*/
void Enviroment::rmvSmallBubbles()
{
    int i=0;
    while(enemies[i]!=NULL) {
        if(enemies[i]->getVolume()<1) {
            enemies[i]=NULL;
        }
        i++;
    }
}

/** Bubble maintenance
*
*   call function related to the maintenance of the correct enemy count 
*
*/
void Enviroment::bubbleMaintenance()
{
    rmvSmallBubbles();
    bubbleRegen();
}

/** end Game Conditions
*
*   Checks whether the player has left the screen or is below the volume 
*   at which it can no longer effect the game. If it has it, the enemy bubbles 
*   are destroyed and the player bubble is reset.
*
*/
void Enviroment::endGameConditions()
{
    int x = player->getcpx();
    int y = player->getcpy();
    if((x<0||x>47)||(y<0||y>81)) {
        deadBubble=TRUE;
        player = new Bubble(24,42,3,0,0,1);
        destroyBubbles();
    }
    float vol =player->getVolume();
    if(vol<1)
    {
        deadBubble=TRUE;
        player = new Bubble(24,42,3,0,0,1);
        destroyBubbles();   
    }
    if(restart)
    {
        player = new Bubble(24,42,3,0,0,1);
        destroyBubbles();
        score = player->getVolume();
        restart=FALSE;          
    }
}

/** score update
*
*   if the volume of the player is larger than it has been before update score 
*   with volume
*
*/
void Enviroment::scoreUpdate()
{
    if(player->getVolume()>score)score=player->getVolume();  
}

/** Enviroment Constructor
*
*   creates the game enviroment with the number of enemy bubbles made being 
*   being passed as a paramter. All members of the enemy bubble array are set to
*   NULL first for reliable comparisons in the future. 
* 
*/
Enviroment::Enviroment(int n)
{
    player = new Bubble(24,42,3,0,0,1);

    ncircles = n;
    for(int i=0; i<4; i++) {
        enemies[i]= NULL;
    }
    for(int i=0; i<n; i++) {
        enemies[i] = new Bubble(0);
    }
}

/** update Enviroment (gameloop)
*
*   calls all the functions needed for the game engine.
*
*/
void Enviroment::update()
{
    int i=0;
    while(enemies[i]!=NULL) {
        enemies[i]->update();
        dstryOffScreenBs();
        i++;
    }
    player->update();
    player->changeVelocity();
    gravityAccelApply(i);
    collisionDetect();
    bubbleMaintenance();
    scoreUpdate();
    endGameConditions();
}

/** collision Detect 
*
*   calls fucnions that deal with the two insances of collision detect, between 
*   two enemies and bewen the player and an enemy. 
*
*/
void Enviroment::collisionDetect()
{
    plyrCollideOther();
    otherCollideOther();
}

/** player Collide Other
*
*   checks whether a player has collided with an through the distance between 
*   the two bubbles being less than the sum of their radius if so, a mass 
*   transfer fucnion is called between the two bubbles is they have collided. 
*   Another check it carries out is whether one bubble is completely engulfed by
*   another, if so an instant mass transfer fucntion is called. 
*   
*/
void Enviroment::plyrCollideOther()
{
    int x2, y2;
    float r2, rsum, rdlta, dist;
    
    float r1= player->radius;
    int x1= player->x-r1;
    int y1= player->y-r1;

    int i=0;
    while(enemies[i]!=NULL) {
        r2= enemies[i]->radius;
        x2= enemies[i]->x-r2;
        y2= enemies[i]->y-r2;
        rsum= r2+r1;// sum of circles radius
        rdlta= r1-r2;// sum of plyr diameter an others radius
        dist= sqrt(pow(x2-x1,2.0f)+pow(y2-y1,2.0f));

        if((r1>r2)&&(dist<rdlta)) {
            pad.led(2,0.0f);pad.led(5,0.0f);
            //printf("inst::  rdlta=%f, rsum=%f   ,dist=%f\n",rdlta,rsum,dist);
            instMassTransfer(i);
        } else if(dist<rsum) {
            pad.led(2,10.0f);pad.led(5,0.0f);
            massTransfer(i);
            //printf("grad::  rdlta=%f, rsum=%f   ,dist=%f\n",rdlta,rsum,dist);
        }else{
            pad.led(1,0.0f);pad.led(4,0.0f);
            pad.led(3,0.0f);pad.led(6,0.0f);
            pad.led(2,1.0f);pad.led(5,1.0f);
        }
        i++;
    }
}

/** other Collide Other
*
*   checks whether the enemy bubbles have collieded through the distance between 
*   the two bubbles being less than the sum of their radius if so, a mass 
*   transfer fucnion is called between the two bubbles is they have collided. 
*   Another check it carries out is whether one bubble is completely engulfed by
*   another, if so an instant mass transfer fucntion is called. 
*   
*/
void Enviroment::otherCollideOther()
{
    float r1, r2, rsum, rdlta, dist;
    int x1, y1, x2, y2;
    
    for(int i=0; i<3; i++) {
        r1= enemies[i]->radius;
        x1= enemies[i]->x-r1;
        y1= enemies[i]->y-r1;
        for(int j=i+1; j<4;j++) {
            r2= enemies[j]->radius;
            x2= enemies[j]->x-r1;
            y2= enemies[j]->y-r1;
            rsum= r2+r1;// sum of circles radius
            rdlta= r1-r2;// sum of plyr diameter an others radius
            dist= sqrt(pow(x2-x1,2.0f)+pow(y2-y1,2.0f));
            
            if((enemies[i]!=NULL)&&(enemies[j]!=NULL)){
                //printf("voli=%f, volj=%f\n",enemies[i]->getVolume(),enemies[j]->getVolume());
                if((r1>r2)&&(dist<rdlta)) {
                    //printf("inst::  rdlta=%f, rsum=%f   ,dist=%f\n",rdlta,rsum,dist);
                    instMassTransfer(i,j);
                } else if(dist<rsum) {
                    massTransfer(i,j);
                    //printf("oograd::  rdlta=%f, rsum=%f   ,dist=%f\n",rdlta,rsum,dist);
                }
            }
        }
    }
}

/** Mass transfer ( player enemy)
*
*   Determines between the three scenarios of mass transfer between player
*   and enemy.
*
*   @param b - position of the enemy bubble in the enemies array. 
*/
void Enviroment::massTransfer(int b)
{
    float dVol=5.0f; // mass transfered per tick
    float volp =player->getVolume();
    float volb =enemies[b]->getVolume();
    bool antiB=enemies[b]->antiBub;
    //printf("volp=%f, volb=%f\n",volp,volb);
    //printf("radp=%f, radb=%f\n",player->radius,enemies[b]->radius);
    //printf("player vol=%f  , player rad=%f\n",volp,rp);
    
    if((volp>volb)&&(!antiB)) { 
        enemies[b]->setRadFromVol((volb-dVol)); 
        player->setRadFromVol((volp+dVol));
        pad.tone(500,0.2); // high pictch tone, indicaing success
        pad.led(3,1.0f);pad.led(6,1.0f); // green leds on, indicaing success
    }  else if(volp<volb&&!antiB) {
        enemies[b]->setRadFromVol(volb+dVol);
        player->setRadFromVol(volp-dVol);
        pad.tone(100,0.2); // low pitch, indicating failure
        pad.led(1,1.0f);pad.led(4,1.0f); // red lights on, inicating failure
    } else if(antiB) {
        enemies[b]->setRadFromVol(volb-dVol);
        player->setRadFromVol(volp-dVol);
        pad.tone(100,0.2); // low pitch, indicating failure
        pad.led(1,1.0f);pad.led(4,1.0f);// red lights on, inicating failure
    }
}

/** mass transfer (other other)
*
*   Deals with the six cases of mass transfer between enemy bubbles.
*
*   @param a - position of enemy in enemies array 
*   @param b - position of enemy in enemies array 
*
*/
void Enviroment::massTransfer(int a,int b)
{
    float vold=5; // mass transfered per tick
    float volb =enemies[b]->getVolume();
    float vola =enemies[a]->getVolume();
    bool bothAB=enemies[a]->antiBub&&enemies[b]->antiBub;
    bool bothNorm=(!enemies[a]->antiBub)&&(!enemies[b]->antiBub);
    
    if(bothNorm&&vola>volb){ 
        enemies[a]->setRadFromVol(vola+vold);
        enemies[b]->setRadFromVol(volb-vold);
    }else if(bothNorm&&volb>vola){
        enemies[a]->setRadFromVol(vola-vold);
        enemies[b]->setRadFromVol(volb+vold);
    }else if(bothAB&&vola>volb){
        enemies[a]->setRadFromVol(vola-vold);
        enemies[b]->setRadFromVol(volb+vold);   
    }else if(bothAB&&volb>vola){
        enemies[a]->setRadFromVol(vola+vold);
        enemies[b]->setRadFromVol(volb-vold);
    }else if(vola>volb){
        enemies[a]->setRadFromVol(vola-vold);
        enemies[b]->setRadFromVol(volb-vold);        
    }else if(volb>vola){
        enemies[a]->setRadFromVol(vola-vold);
        enemies[b]->setRadFromVol(volb-vold);        
    }
}

/** instant mass transfer ( player enemy )
*
*   determines which of the four cases of instant mass transfer should be 
*   applied between an player and an enemy. 
*
*   @param b - position of enemy in enemies array 
*/
void Enviroment::instMassTransfer(int b)
{
    float volp =player->getVolume();
    float volb =enemies[b]->getVolume();
    bool antiB=enemies[b]->antiBub;

    if((volp>volb)&&(!antiB)) {
        player->setRadFromVol(volb+volp);
        pad.tone(500,0.2); // high pitch tone, indicaing success
        pad.led(3,1.0f);pad.led(6,1.0f); // green leds on, indicaing success
        destroyBubble(b);
    } else if((volp>volb)&&(antiB)) {
        player->setRadFromVol(volp-volb);
        pad.tone(100,0.2);// low pitch, indicating failure
        pad.led(1,1.0f);pad.led(4,1.0f); // red lights on, inicating failure
        destroyBubble(b);
    } else if(volp<volb&&!antiB) {
        enemies[b]->setRadFromVol(volb+volp);
        pad.tone(100,0.2);// low pitch, indicating failure
        pad.led(1,1.0f);pad.led(4,1.0f); // red lights on, inicating failure
        player->setRadFromVol(0);
    } else if(volp<volb&&antiB) {
        enemies[b]->setRadFromVol(volb-volp);
        pad.tone(100,0.2);// low pitch, indicating failure
        pad.led(1,1.0f);pad.led(4,1.0f); // red lights on, inicating failure
        player->setRadFromVol(0);
    }
}

/** instant mass transfer (enemy enemy)
*
*   determines which of the 5 cases of instant mass transfer should be 
*   applied between an enemy and an enemy. 
*
*   @param a - position of enemy in emeies array 
*   @param b - position of enemy in enemies array 
*/
void Enviroment::instMassTransfer(int a,int b)
{
    float vola =enemies[a]->getVolume();
    float volb =enemies[b]->getVolume();
    bool bothAB=enemies[a]->antiBub&&enemies[b]->antiBub;
    bool bothNorm=(!enemies[a]->antiBub)&&(!enemies[b]->antiBub);
    
    if(bothNorm&&vola>volb){
        enemies[a]->setRadFromVol(vola+volb);
        destroyBubble(b);
    }
    else if(bothNorm&&volb>vola){
        enemies[b]->setRadFromVol(vola+volb);
        destroyBubble(a);
    }
    else if(bothAB){ 
        enemies[a]->setRadFromVol((vola+volb)/2);
        enemies[b]->setRadFromVol((vola+volb)/2);
    }
    else if(vola>volb){
        enemies[a]->setRadFromVol(vola-volb);
        destroyBubble(b);    
    }
    else if(volb>vola){
        enemies[b]->setRadFromVol(volb-vola);
        destroyBubble(a);
    }
}


/** distance between bubbles (player and enemy) 
*
*   returns the distance between a player and the enemy. The passed integer
*   indicates which enemy to evaulate from the enemies array. Distance is 
*   calculated using the pythagorean thereom
*
*   @param int i - position of enemy in enemies array
*   @retuns float - distance between the two bubbles
*/
float Enviroment::distBtwnBubs(int i)
{
    float xd =(player->getcpx())-(enemies[i]->getcpx());
    float yd =(player->getcpy())-(enemies[i]->getcpy());
    //printf("%f\n",sqrt(pow(xd,2)+pow(yd,2)));
    return sqrt(pow(xd,2)+pow(yd,2));
}

/** distance between bubbles (enemy and enemy) 
*
*   returns the distance between a two enemys. the passed integers
*   indicates which enemies to evaulate from the enemies array. Distance is 
*   calculated using the pythagorean thereom
*
*   @param int i - position of enemy 1 in enemies array
*   @param int j - position of enemy 2 in enemies array
*   @retuns float - distance between the two bubbles
*/
float Enviroment::distBtwnBubs(int i,int j)
{
    float xd =(enemies[i]->getcpx())-(enemies[j]->getcpx());
    float yd =(enemies[i]->getcpy())-(enemies[j]->getcpy());
    //printf("%f\n",sqrt(pow(xd,2)+pow(yd,2)));
    return sqrt(pow(xd,2)+pow(yd,2));
}

/** angle between bubbles (player and enemy )
*
*   returns the angle between the two bubbles. The passed integer indicates 
*   which enemy to evaulate from the enemies array. Angles are calclated using
*   trigonometric fucntions.
*
*   @param i - postion of enemy in enemies array
*   @returns float - angle between player and enemy
*/
float Enviroment::angleBtwnBubs(int i)
{
    float adj = (player->y)-(enemies[i]->y);
    float opp = (player->x)-(enemies[i]->x);
    //printf("%f",atan(opp/adj));
    return atan(opp/adj);
}

/** angle between bubbles (enemey and enemy )
*
*   returns the angle between the two bubbles. The passed integers indicate 
*   which enemies to evaulate from the enemies array. Angles are calclated using
*   trigonometric fucntions.
*
*   @param i - postion of enemy 1 in enemies array
*   @param j - postion of enemy 2 in enemies array
*   @returns float - angle between enemies
*/
float Enviroment::angleBtwnBubs(int i,int j)
{
    float adj = (enemies[i]->y)-(enemies[j]->y);
    float opp = (enemies[i]->x)-(enemies[j]->x);
    //printf("%f",atan(opp/adj));
    return atan(opp/adj);
}

/** Gravitational force between bubbles (player enemy)
*
*   returns the gravitational force between player and enemy at passed postion
*   in array. calcualted using,
*   
*            Gf = (Gc*MASSa*MASSb)/distance^2 (volume used for mass)
*   
*   @param i - position of enemy in enemies array  
*   @returns float - gravitational force between player and enemy  
*/
float Enviroment::gravForceBtwnBubs(int b)
{
    float dist = distBtwnBubs(b);
    float volp = player->getVolume();
    float volb = enemies[b]->getVolume();
    if(dist>player->radius+enemies[b]->radius) // removes graviy if collieded
        return (GRAVCONSTANT*volp*volb)/pow((float)dist,2.0f);
    else return 0;
}

/** Gravitational force between bubbles (enemy enemy)
*
*   returns the gravitational force between enesmies at passed postions in array
*   Calcualted using,
*   
*            Gf = (Gc*MASSa*MASSb)/distance^2 (volume used for mass)
*   
*   @param i - position of enemy 1 in enemies array
*   @param j - position of enemy 2 in enemies array   
*   @returns float - gravitational force between enemies  
*/
float Enviroment::gravForceBtwnBubs(int a,int b)
{
    float dist = distBtwnBubs(a,b);
    float volp =enemies[a]->getVolume();
    float volb =enemies[b]->getVolume();
    if(dist>enemies[a]->radius+enemies[b]->radius)// removes graviy if collieded
        return (GRAVCONSTANT*volp*volb)/pow(dist,2.0f);
    else return 0;
}

/** apply Gravity acceleration 
*
*   calls functions to apply acceleration due to gravity between all bubbles 
*
*/
float Enviroment::gravityAccelApply(int i)
{
    gravityPlyrEnem();
    gravityEnemEnem();
}

/** Gavity Player Enemy 
*
*   Determines in which direction the acceleration due to gavity should be in 
*   based of the postion of the bubbles interacting. Passes this info to 
*   function calculating magnitude of force and applying acceleration.  
*   
*/
void Enviroment::gravityPlyrEnem()
{
    float bxcp, bycp; 
    float pxcp = player->getcpx();
    float pycp = player->getcpy();
    int i=0;
    while(enemies[i]!=NULL)
    {
        bxcp = enemies[i]->getcpx();
        bycp = enemies[i]->getcpy();
        if(pxcp<bxcp&&pycp<bycp) gravForceDirMod(0,i); //bottom right
        if(pxcp>bxcp&&pycp<bycp) gravForceDirMod(1,i); //top right
        if(pxcp<bxcp&&pycp>bycp) gravForceDirMod(2,i); //bottom left
        if(pxcp>bxcp&&pycp>bycp) gravForceDirMod(3,i); //top left    
        i++;
    }
    //printf("p=%f,%f   b=%f,%f\n",pxcp,pycp,bxcp,bycp); 
}

/** Gavity Enemy Enemy 
*
*   Determines in which direction the acceleration due to gavity should be in 
*   based of the postion of the bubbles interacting. Passes this info to 
*   function calculating magnitude of force and applying acceleration.  
*   
*/
void Enviroment::gravityEnemEnem()
{
    float b1xcp, b1ycp, b2xcp, b2ycp;
    
    for(int i=0;i<3;i++){
        b1xcp = enemies[i]->getcpx();
        b1xcp = enemies[i]->getcpy();
        for(int j=i+1;j<4;j++){
            b2xcp = enemies[j]->getcpx();
            b2ycp = enemies[j]->getcpy();
            if((enemies[i]!=NULL)&&(enemies[j]!=NULL))
            {
                if(b1xcp<b2xcp&&b1ycp<b2ycp) gravForceDirMod(0,i,j); //bottom right
                if(b1xcp>b2xcp&&b1ycp<b2ycp) gravForceDirMod(1,i,j); //top right
                if(b1xcp<b2xcp&&b1ycp>b2ycp) gravForceDirMod(2,i,j); //bottom left
                if(b1xcp>b2xcp&&b1ycp>b2ycp) gravForceDirMod(3,i,j); //top left
            }
        }
    }
    //printf("p=%f,%f   b=%f,%f\n",pxcp,pycp,bxcp,bycp); 
}

/** Gravity Force Direction Modificaion
*
*   Calculates and normalizes the x and y components of graviational force using
*   trigonomeric fucntions, converts these to accelerations using F=MA , then  
*   changes directions of the accelerations and applies then to the velocities
*   of involved bubbles. (mass substituted for volume)
*   
*   @param i -  indicates which direction accel should be applied in
*   @param b - indicates enemy bubble to apply accel to. 
*
*/
void Enviroment::gravForceDirMod(int i,int b)
{
    // normalised x and y components of gravity
    float xCompGF = sqrt(pow(gravForceBtwnBubs(b)*sin(angleBtwnBubs(b)),2.0f));
    float yCompGF = sqrt(pow(gravForceBtwnBubs(b)*cos(angleBtwnBubs(b)),2.0f));
    // x and y compoents of acceleration
    float xAccelP = xCompGF/player->getVolume();
    float yAccelP = yCompGF/player->getVolume();
    float xAccelB = xCompGF/enemies[b]->getVolume();
    float yAccelB = yCompGF/enemies[b]->getVolume();
    
    if(i==1) { //printf("top right\n");
        player->xv-=xAccelP*dt2;
        player->yv+=yAccelP*dt2;
        enemies[b]->xv+=xAccelB*dt2;
        enemies[b]->yv-=yAccelB*dt2;
    }
    if(i==0) { //printf("bottom right\n");
        player->xv+=xAccelP*dt2;
        player->yv+=yAccelP*dt2;
        enemies[b]->xv-=xAccelB*dt2;
        enemies[b]->yv-=yAccelB*dt2;
    }
    if(i==3) { //printf("top left\n");
        player->xv-=xAccelP*dt2;
        player->yv-=yAccelP*dt2;
        enemies[b]->xv+=xAccelB*dt2;
        enemies[b]->yv+=yAccelB*dt2;
    }
    if(i==2) { //printf("bottom left\n");
        player->xv+=xAccelP*dt2;
        player->yv-=yAccelP*dt2;
        enemies[b]->xv-=xAccelB*dt2;
        enemies[b]->yv+=yAccelB*dt2;
    }
    //printf("plyr x=%f plyr y=%f bub x=%f, bub y=%f\n",xAccelP,yAccelP,xAccelB,yAccelB);
    //printf("XCompGF=%f,YCompGF=%f\n",xCompGF,yCompGF);
    //printf("angle=%f,  force =%f\n\n",angleBubPlyr(b),gravForcePlyrBub(b));
    //float vel = sqrt((player->xv*player->xv)+(player->yv*player->yv));
    //printf("pvel= %f\n",vel);
}

/** Gravity Force Direction Modificaion
*
*   Calculates and normalizes the x and y components of graviational force using
*   trigonomeric fucntions, converts these to accelerations using F=MA , then  
*   changes directions of the accelerations and applies then to the velocities
*   of involved bubbles. (mass substituted for volume)
*   
*   @param i -  indicates which direction accel should be applied in
*   @param b - indicates enemy bubble to apply acceleration to. 
*   @param a - indicates enemy bbble to apply acceleration to. 
*
*/
void Enviroment::gravForceDirMod(int i,int a,int b)
{
    float xCompGF = sqrt(pow(gravForceBtwnBubs(a,b)*sin(angleBtwnBubs(a,b)),2.0f));
    float yCompGF = sqrt(pow(gravForceBtwnBubs(a,b)*cos(angleBtwnBubs(a,b)),2.0f));
    float xAccelA = xCompGF/enemies[a]->getVolume();
    float yAccelA = yCompGF/enemies[a]->getVolume();
    float xAccelB = xCompGF/enemies[b]->getVolume();
    float yAccelB = yCompGF/enemies[b]->getVolume();
    if(i==1) { //printf("top right\n");
        enemies[a]->xv-=xAccelA*dt2;
        enemies[a]->yv+=yAccelA*dt2;
        enemies[b]->xv+=xAccelB*dt2;
        enemies[b]->yv-=yAccelB*dt2;
    }
    if(i==0) { //printf("bottom right\n");
        enemies[a]->xv+=xAccelA*dt2;
        enemies[a]->yv+=yAccelA*dt2;
        enemies[b]->xv-=xAccelB*dt2;
        enemies[b]->yv-=yAccelB*dt2;
    }
    if(i==3) { //printf("top left\n");
        enemies[a]->xv-=xAccelA*dt2;
        enemies[a]->yv-=yAccelA*dt2;
        enemies[b]->xv+=xAccelB*dt2;
        enemies[b]->yv+=yAccelB*dt2;
    }
    if(i==2) { //printf("bottom left\n");
        enemies[a]->xv+=xAccelA*dt2;
        enemies[a]->yv-=yAccelA*dt2;
        enemies[b]->xv-=xAccelB*dt2;
        enemies[b]->yv+=yAccelB*dt2;
    }
    //printf("plyr x=%f plyr y=%f bub x=%f, bub y=%f\n",xAccelP,yAccelP,xAccelB,yAccelB);
    //printf("XCompGF=%f,YCompGF=%f\n",xCompGF,yCompGF);
    //printf("angle=%f,  force =%f\n\n",angleBubPlyr(a,b),gravForcePlyrBub(a,b));
    //float vel = sqrt((player->xv*player->xv)+(player->yv*player->yv));
    //printf("pvel= %f\n",vel);
}
