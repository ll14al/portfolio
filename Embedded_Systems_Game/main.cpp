#include <stdio.h>
#include <string>
#include <stddef.h>
#include "mbed.h"
#include "Gamepad.h"
#include "N5110.h"
#include "bubble.h"
#include "enviroment.h"
#include "FXOS8700CQ.h"
#include "menu.h"

#define TRUE 1
#define FALSE 0
#define PI 3.1415926535
#define ACCEL 7
#define GRAVCONSTANT 6.6738480 * pow(10.0,-2.0)

//global variables
double dt2=1.0/7.0; //used in physics functions as time step constant
bool deadBubble=FALSE;
bool restart=FALSE;
float score=0.0;
bool crossPadEnabled=TRUE; // used for toggling cross pad controls
bool anlgStickEnabled=TRUE; // used for toggling ananlogue stick controls
bool aclrmtrEnabled=TRUE;   // used for toggling acceleromter controls

//Global objects
Serial pc(USBTX,USBRX); // serial output for debugging
N5110 lcd(PTC9,PTC0,PTC7,PTD2,PTD1,PTC11); // lcd screen 
FXOS8700CQ aclromtr(I2C_SDA,I2C_SCL); // acceleromter
Data values; 
Gamepad pad;

//Function prototypes
void welcome();
void init();
void clear();
void gameOver();

int main(int argc, char* args[])
{
    pc.baud (9600);
    init();
    welcome();
    
    Enviroment *env = new Enviroment(2);
    Menu *m = new Menu();

    while (1) {
        
        lcd.clear();
        if(pad.check_event(Gamepad::START_PRESSED) == TRUE)m->menuP();
        env->update();
        if(deadBubble)gameOver();
        lcd.refresh();
    }
    
    
    return 0;
}



/** initialize objects
*
*   Contains the initialization of objects and seeding of rand fucntion
*   required by the game. 
*
*/
void init()
{
    lcd.init();
    pad.init();
    aclromtr.init();
    srand(time(NULL)); //seeds rand function
}


/** welcome screen
*   
*   Creates welcome screen sate for game including game title and abillity 
*   to progress from the welcome screen by pressing start. Flashes leds also.
*
*/
void welcome()
{
    lcd.printString("    Bubble    ",0,1);
    lcd.printString("    Battles    ",0,2);
    lcd.printString("  Press Start ",0,4);
    lcd.refresh();

    // wait flashing LEDs until start button is pressed
    while (pad.check_event(Gamepad::START_PRESSED) == false) {
        pad.leds_on();
        wait(0.1);
        pad.leds_off();
        wait(0.1);
    }
    wait(1);
    lcd.clear();
    lcd.refresh();
    
    
}

/** Game over screen
*
*   is called when the player dies. Displays indication the game is over and the
*   final score of the player. If start is pressed the game is restarted and
*   score is set to zero for the next game.  
*
*   @param - 
*/
void gameOver()
{
    score-=113;
    int loop=1;
    while(loop) {
        if(pad.check_event(Gamepad::START_PRESSED) == TRUE)loop=0;
        lcd.clear();
        lcd.printString("  GAME OVER!  ",0,1);
        lcd.printString(" FINAL VOLUME ",0,3);
        char str[20];
        sprintf (str,"  %0.0f",score);
        lcd.printString(str,10,4);
        lcd.refresh();
        if(pad.check_event(Gamepad::START_PRESSED) == TRUE)loop=0;
        wait(1);lcd.clear();wait(0.5);
        lcd.printString(" PRESS START  ",0,1);
        lcd.printString(" TO TRY AGAIN ",0,3);
        lcd.refresh();wait(1);
        if(pad.check_event(Gamepad::START_PRESSED) == TRUE)loop=0;
    }
    score=0;
    deadBubble=FALSE;
}



