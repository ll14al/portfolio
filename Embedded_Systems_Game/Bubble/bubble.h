#ifndef CIRCLE_H
#define CIRCLE_H

#include "mbed.h"
#include "N5110.h"
#include "Gamepad.h"
#include "FXOS8700CQ.h"

#define TRUE 1
#define FALSE 0
#define PI 3.1415926535
#define ACCEL 7
#define GRAVCONSTANT 6.6738480 * pow(10.0,-2.0)

extern double dt2;
extern N5110 lcd;
extern Data values;
extern FXOS8700CQ aclromtr;
extern Gamepad pad;
extern float score;
extern bool crossPadEnabled;
extern bool anlgStickEnabled;
extern bool aclrmtrEnabled;

/** Bubble Class
*
*   This class represents the objects that make up the player and ememies in the
*   game. Contained in this class are the methods for drawing, generating and 
*   controling (if needed) the object.  
*  
*   @author Andrew J Lumsden
*/

class Bubble
{

public:
    Bubble();
    Bubble(char ip);
    Bubble(float x1, float y1, float r, float xv1, float yv1, char ip);
    ~Bubble();
 
    float x, y;    // location
    float radius;  // size
    float xv, yv;  // directional velocity
    char isplayer; 
    bool antiBub; 
 
    void changeVelocity();
    void update();
    float getcpx(); // gets the center x point of the circle 
    float getcpy(); // gets the center y point of the cicrle 
    float getVolume();
    void setRadFromVol(float vol);
    int baseRadiusMod();
    void control();
    void drawCircle();
    void drawFilledCircle();
    void velGen(int s);
    void randEdgeEntry();
    void radiusGen();
    void crossPadVel();
    void anStickVel();
    void aclrmtrVel();
    

    
private:
    
    
 
};
#endif