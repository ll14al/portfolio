#include "bubble.h"

/** Control
*
*   Changes the location of the circle based of its velociy. 
*
*/
void Bubble::control()
{
    x=x+xv;
    y=y+yv;
}

/** draw Circle 
*
*   draws a the circle on the screen. Uses the circles radius and location 
*   coordinates to do this. 
*
*/
void Bubble::drawCircle()
{
    //lcd.drawCircle(getcpy(),getcpx(), radius,FILL_TRANSPARENT); crashes game
    double i, angle, x1, y1;
    char h, w, sx, sy;

    for(i=0; i<360; i+=1) {
        angle = i;
        x1 = radius * cos(angle * PI / 180);
        y1 = radius * sin(angle * PI / 180);
        h = int(rint(x1));
        w = int(rint(y1));
        sx = char(rint((h+x-radius)));
        sy = char(rint((w+y-radius)));
        if((sx<48)&&(sy<84)) {
            if((sx>-1)&&(sy>-1)) {
                lcd.setPixel(sy,sx);
            }
        }
    }
}

/** draw filled Circle 
*
*   draws a filled circle on the screen. Uses the circles radius and location 
*   coordinates to do this. 
*
*/
void Bubble::drawFilledCircle()
{
    //lcd.drawCircle(getcpy(),getcpx(), radius,FILL_BLACK); crashes program
    double i, angle, x1, y1;
    char h, w, sx, sy;

    for(i=0; i<360; i+=6) {
        angle = i;
        for(int r=0; r<radius+1; r++) {
            x1 = r * cos(angle * PI / 180);
            y1 = r * sin(angle * PI / 180);
            h = int(rint(x1));
            w = int(rint(y1));
            sx = char(rint((h+x-radius)));
            sy = char(rint((w+y-radius)));
            if((sx<48)&&(sy<84)) {
                if((sx>-1)&&(sy>-1)) {
                    lcd.setPixel(sy,sx);
                }
            }
        }
    }
    
}

/** get Volume 
*
*   calculates and returns the volume of the Bubble. 
*   
*   @return float
*/
float Bubble::getVolume()
{
    return ((4.0/3.0)*(PI)*(pow(radius,3.0f)));        
}

/** set Radius from Volume
*
*   calculates and returns the volume of the Bubble. 
*   
*   @return float
*/
void Bubble::setRadFromVol(float vol)
{
    radius=cbrt((3.0f*vol)/(4.0f*PI));   
}

/** velocity Generator 
*
*   Generates a random velociy for the bubbles entering from off screen 
*   
*   @param s - indicates which side the bubble will be enetering from.
*/
void Bubble::velGen(int s){
    
    if(s==0) { //lower edge
        xv = rand() % 200;
        xv = (xv/300);
        yv = rand() % 400;
        yv = ((yv-300)/300);
    }
    if(s==1) { //top edge
        xv = rand() % 200;
        xv = (xv/300)*-1;
        yv = rand() % 400;
        yv = ((yv-300)/300);
    }
    if(s==2) { // left side
        xv = rand() % 400;
        xv = ((xv-300)/300);
        yv = rand() % 200;
        yv = (yv/300);
    }
    if(s==3) { // right side
        xv = rand() % 400;
        xv = ((xv-300)/300);
        yv = rand() % 200;
        yv = (yv/300)*-1;
    }
    //float vel=sqrt(pow(xv,2.0)+pow(yv,2.0));
    //printf("xv=%f  ,yv=%f  ,s=%d  ,vel=%f\n",xv,yv,s,vel);
}

/** randon Edge Entry 
*
*   Generates a position for the buble to enter from and passes this to a  
*   velocoty generator fuction. 
*
*/
void Bubble::randEdgeEntry()
{
    int s = rand()%4;
    if(s==0) { // top edge
        x = 1;
        y = rand() % 81;
        velGen(s);
        //xv = 0.2, yv = 0;
    }
    if(s==1) { // bottom edge
        x = 47;
        y = rand() % 81;
        velGen(s);
        //xv = -0.2, yv = 0;
    }
    if(s==2) { // left side
        x = rand() % 47;
        y = 1;
        velGen(s);
        //xv = 0, yv = 0.2;
    }
    if(s==3) { // right side
        x = rand() % 47;
        y = 81;
        velGen(s);
        //xv = 0, yv = -0.2;
    }
    //printf("s=%d\n",s);
}

/** radius Generator
*
*   Generates a radius for when enemy bubbles are being constructed. Smaller
*   radius' have a higher chance of creation.
*
*/
void Bubble::radiusGen()
{    
    int a = baseRadiusMod();
    int n = rand() % 10;
    if(n==0||n==1||n==2||n==3)radius=a+2;
    if(n==4||n==5||n==6) radius=a+3;
    if(n==7||n==8) radius = a+4;
    if(n==9) radius = a+6;
    //printf("radius=%0.1f\n",radius);
}

/** base radius modifier
*
*   This fucntion changes the base radius from which the eenmy bubbles are 
*   gernerated from. The base radius is set depending on the score the size of
*   the player. This introduced  dynamic difficulty into the game. 
*
*/
int Bubble::baseRadiusMod()
{
    if(score>200) return 1;
    if(score>300) return 2;
    if(score>500) return 3;
    return 0;
} 

/** anti Bubble derermination
*
*   This function determines the possibilty that new bubbles are anti-bubbles. 
*   The possibility is determined by the score of the player, adding dynamic
*   difficulty to the game.   
*
*/
bool antiBubDeterm()
{
    int a=0; 
    if(score<400)a=1;
    if(score<800)a=2;
    if(score<2000)a=3;
    int s = rand() % 5;
    if(s<=a)return FALSE;
    else return TRUE;
}

Bubble::Bubble()
{
    ;
}

/** Bubble consructor
*
*   This constructor is called for generating enemy bubbles. Other fucntions are 
*   called to set the values of the bubble beig contructed. 
*   
*   @param ip - indicates whether the bubble is the players bubble or not. 
*/
Bubble::Bubble(char ip)
{
    xv=0,yv=0,x=0,y=0;
    isplayer=ip;
    randEdgeEntry();
    radiusGen();
    antiBub = antiBubDeterm();
}

/** Bubble consructor
*
*   This constructor is used for the players bubble, with all atibutes being set
*   from parameters. 
*
*   @param x1 - location, x coordinate
*   @param y1 - location, y coordinate
*   @param r -  radius  
*   @param xv1 - velocity, x component 
*   @param yv1 - velocity, y component
*   @param ip - is player or not
*/
Bubble::Bubble(float x1, float y1, float r, float xv1, float yv1, char ip)
{
    x = x1;
    y = y1;
    radius = r;
    xv = xv1;
    yv = yv1;
    isplayer = ip;
    antiBub=FALSE;
}

/** changeVelocity 
*
*   calls all the fucntions that deal with the users input for effecting the
*   bubbles velociy.
*
*/
void Bubble::changeVelocity()
{
    if(crossPadEnabled)crossPadVel();
    if(anlgStickEnabled)anStickVel();
    if(aclrmtrEnabled)aclrmtrVel();
}

/** cross pad velociy control
*
*   looks for input from the cross pad (A, B, X and Y buttons) and changes the 
*   players velocity accordingly. Changes the velocity by adding the 
*   acceleration constant * timestep as velocity=acceleration*time. 
* 
*/
void Bubble::crossPadVel()
{
    if(pad.check_event(Gamepad::Y_PRESSED) == true) xv+= -ACCEL*dt2;     // up -
    else if(pad.check_event(Gamepad::A_PRESSED) == true) xv+= ACCEL*dt2; // down
    else if(pad.check_event(Gamepad::X_PRESSED) == true) yv+= -ACCEL*dt2;// left -
    else if(pad.check_event(Gamepad::B_PRESSED) == true) yv+=ACCEL*dt2; // right
    
}

/** ananlog stick velociy control
*
*   looks for input from the analog stick and changes the 
*   players velocity accordingly. Changes the velocity by adding the 
*   acceleration constant * timestep as velocity=acceleration*time. Deals with 
*   8 diferent directions N NE E SE S SW  W NW and the acceleration value is
*   aquired from the acceleration*magnitude of the ananlog stick. 
*
*/
void Bubble::anStickVel()
{
    Direction dir = pad.get_direction();
    if(dir==N){//printf("NORTH\n");
        xv=-ACCEL*dt2;yv=0;  // velocity modification
    }else if(dir==NE){//printf("NORTH EAST\n");
        xv=-ACCEL*dt2;yv=ACCEL*dt2; // velocity modification    
    }else if(dir==E){//printf("EAST\n");
        xv=0;yv=ACCEL*dt2; // velocity modification
    }else if(dir==SE){//printf("SOUTHEAST\n");
        xv=ACCEL*dt2;yv=ACCEL*dt2; // velocity modification
    }else if(dir==S){//printf("SOUTH\n");
        xv=ACCEL*dt2;yv=0; // velocity modification
    }else if(dir==SW){//printf("SOUTHWEST\n");
        xv=ACCEL*dt2;yv=-ACCEL*dt2; // velocity modification
    }else if(dir==W){//printf("WEST\n");
        xv=0;yv=-ACCEL*dt2; // velocity modification
    }else if(dir==NW){//printf("NORTHWEST\n");
        xv=-ACCEL*dt2;yv=-ACCEL*dt2;
   }
}

/** accelarometer effect 
*
*   reads the values from the accelaometer and uses these to effect the velocity 
*   of the players bubble. The interacion is such that if the game pads left 
*   side is lifted the bubble will move to the left, mimicing a sprit level. 
*
*/
void Bubble::aclrmtrVel()
{
    values = aclromtr.get_values();
    //printf("ax=%f,  ay=%f\n",values.ax,values.ay);
    //printf("1.xv=%f, yv=%f\n",xv,yv);
    float ax = values.ax;
    float ay = values.ay;
    if((ax>0.1f)&&(ay<0.1f&&ay>-0.1f)){
        xv+=0.5f*dt2;
        //printf("SOUTH\n");
    }
    else if((ax<-0.1f)&&(ay<0.1f&&ay>-0.1f)){
        xv+=-0.7f*dt2;
        //printf("NORTH\n");
    }
    else if((ay>0.1f)&&(ax>-0.1f&&ax<0.1f)){
        yv+=0.7f*dt2;
        //printf("EAST\n");
    }
    else if((ay<-0.1f)&&(ax>-0.1f&&ax<0.1f)){
        yv+=-0.7f*dt2;
        //printf("WEST\n");
    }else{
        //printf("center\n");    
    }
    //printf("2.xv=%f, yv=%f\n",xv,yv);
    //printf("ax=%f,  ay=%f\n",values.ax,values.ay);
}

/** update 
*   
*   This function calls all functions which need render and update the bubble 
*   on every tick.  
*
*/
void Bubble::update()
{
    if(antiBub)drawFilledCircle();
    if(!antiBub)drawCircle();
    control();
}

/** get xcp 
*   
*   Get x coord of circle's center.
*
*   @return float 
*/
float Bubble::getcpx(){return x-radius;}

/** get ycp 
*   
*   Get y coord of circle's center.
*
*   @return float 
*/
float Bubble::getcpy(){return y-radius;}

