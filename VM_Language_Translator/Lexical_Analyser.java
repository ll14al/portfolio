import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StreamTokenizer;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

public class Lexical_Analyser 
{
	
	InputStream is;
	Reader r;
	StreamTokenizer st;
	
	public Lexical_Analyser(String filename) throws IOException
	{
		is = new FileInputStream(filename);
		r = new BufferedReader(new InputStreamReader(is));
		st = new StreamTokenizer(r);
	}
	
	public Token getNextToken() throws IOException 
	{
		int t1 = st.nextToken();
		Token outt = null;
		
		if(t1==StreamTokenizer.TT_WORD){ // ignore comments
			String s = st.sval;
			s = s.substring(0, 2);
			if(s.equals("//")){
				int ln = st.lineno();
				while(st.lineno()==ln){
					t1=st.nextToken();
				}
			}
		}
		switch(t1) 
		{
			case StreamTokenizer.TT_EOF: // if end of file
				outt = new Token("EOF");
				break;
			case StreamTokenizer.TT_NUMBER:
				int i = (int)st.nval;
				if(i>=0){
					outt = new Token(i);
				}else{
					System.out.println("LEXICAL ERROR: NEGATIVE INTEGER ECOUNTERED");
					System.exit(0);
				}
				break;
			case StreamTokenizer.TT_WORD:
				outt = new Token(st.sval);
				break; 
		}
		return outt;
	}	
}