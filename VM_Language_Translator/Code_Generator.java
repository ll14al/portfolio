
public class Code_Generator {

	String funcName; // used for localising labels/variables to functions
	String count; // used for making labels unique
	
	public Code_Generator() {
		count="0";
		funcName= " ";
	}
	
	public String nextCount(){
		// used to count number of functions for return addresses
			
		int i = Integer.valueOf(count);
		i++;
		count=String.valueOf(i);
		return count;
	}
	
	public String init(){
		// this function generates code responsible for initialising  
		// the stack pointer and the main function.
		
		String s = "@256\n" +
				   "D=A\n"  +
				   "@SP\n"  +
				   "M=D\n";
		return s;
	}
	
	public String callFunction(String name ,String nArgs ){
		String c = nextCount();
		
		String s = "@SP\n"   +
				   "D=M\n"   +
				   "@R13\n"  +
				   "M=D\n"   +	// save value in stack pointer to R13  
				  
				   "@RET"+c+"\n"+
				   "D=A\n" 	 +
				   "@SP\n"   +
				   "A=M\n"	 +
				   "M=D\n"	 +	// save return address
				   
				   "@SP\n"   +
				   "M=M+1\n" +	// stack pointer ++
				   
				   "@LCL\n"  +
				   "D=M\n"   +
				   "@SP\n"   +
				   "A=M\n"   +	// save calling functions LCL 
				   "M=D\n"   +	// pointer on stack  
				  
				   "@SP\n"   +
				   "M=M+1\n" +	// stack pointer ++
				   
				   "@ARG\n"  +
				   "D=M\n"   +
				   "@SP\n"   +
				   "A=M\n"   +	// save calling functions ARG 
				   "M=D\n"   +	// pointer on stack  
				  
				   "@SP\n"   +
				   "M=M+1\n" +	// stack pointer ++
				   
				   "@THIS\n" +
				   "D=M\n"   +
				   "@SP\n"   +
				   "A=M\n"   +	// save calling functions THIS 
				   "M=D\n"   +	// pointer on stack  
				   
				   "@SP\n"   +
				   "M=M+1\n" +	// stack pointer ++
				   
				   "@THAT\n" +
				   "D=M\n"   +
				   "@SP\n"   +
				   "A=M\n"   +	// save calling functions THAT 
				   "M=D\n"   +	// pointer on stack  
				   
				   "@SP\n"   +
				   "M=M+1\n" +	// stack pointer ++
				   
				   "@R13\n"  +
				   "D=M\n"	 +
				   "@"+nArgs+"\n"+	 
				   "D=D-A\n" +
				   "@ARG\n"  +
				   "M=D\n"   +  // set ARG pointer for called function
				   
				   "@SP\n"	 +
				   "D=M\n"	 +
				   "@LCL\n"  +
				   "M=D\n"	 +
				   "@"+name+"\n"+
				   "0;JMP\n" + // set LCL pointer for called function 
				   "(RET"+c+")\n";
		return s;
	}
	
	public String fucntionDec(String name, String nArgs){
		int nArgi = Integer.valueOf(nArgs);
		
		String s = "("+name+")\n" +
					"@SP\n"  +
					"A=M\n"; // put M[SP] into A register
		
		for(int i=0;i<nArgi;i++){// for all args
			s+=	"M=0\n"   + 	// set contents to zero
				"A=A+1\n";  	// inc pointer
		}
		s+= "D=A\n" +	
			"@SP\n"	+
			"M=D\n"; // point SP to last arg
		
		return s;
	}
	

	
	public String returnFucntion(){
		
		String s = "@LCL\n"	 + 	
				   "D=M\n"	 +	// Frame starts at LCL
				   "@5\n"	 +
				   "A=D-A\n" +	
				   "D=M\n"	 +
				   "@R13\n"	 +
				   "M=D\n"	 +	// return address = frame-5
				   
				   "@SP\n"   +
				   "A=M-1\n" +  // Areg=M[SP]-1
				   "D=M\n"   +	// Dreg=M[Areg]
				   "@ARG\n"  +  
				   "A=M\n"   +
				   "M=D\n"   +  // reposition return value
				   
				   "D=A+1\n" +
				   "@SP\n"   +
				   "M=D\n"   +  // Restore callers SP*
				   
				   "@LCL\n"  +
				   "AM=M-1\n"+
				   "D=M\n"   +
				   "@THAT\n" +
				   "M=D\n"   + // Restore callers THAT*
				   
				   "@LCL\n"  +
				   "AM=M-1\n"+
				   "D=M\n"   +
				   "@THIS\n" +
				   "M=D\n"   +  // Restore callers THIS*
				   
				   "@LCL\n"  +
				   "AM=M-1\n" +
				   "D=M\n"   +
				   "@ARG\n"  +
				   "M=D\n"   +  // Restore callers ARG*
				   
				   "@LCL\n"  +
				   "A=M-1\n" +
				   "D=M\n"   +
				   "@LCL\n"  +
				   "M=D\n"   +  // Restore callers LCL*
				   
				   "@R13\n" +
				   "A=M\n" +
				   "0;JMP\n"; // goto return address 
		
					
		return s;			   
	}
	
	public String push(String seg, String loc){
		String s ="";
		if(seg.equals("local")){
			s= "@LCL\n" +
			   "D=M\n"  +
			   "@"+loc+"\n" +
			   "A=D+A\n"+
			   "D=M\n";		   
		}else if(seg.equals("argument")){
			s= "@ARG\n"+
			   "D=M\n"+
			   "@"+loc+"\n" +
			   "A=D+A\n"+
			   "D=M\n";		   
		}else if(seg.equals("this")){
			s= "@THIS\n"+
			   "D=M\n"+
			   "@"+loc+"\n" +
			   "A=D+A\n"+
			   "D=M\n";
		}else if(seg.equals("that")){
			s= "@THAT\n"+
			   "D=M\n"+
			   "@"+loc+"\n" +
			   "A=D+A\n"+
			   "D=M\n";
		}else if(seg.equals("pointer")){
			if(loc.equals("0")){
				s= "@THIS\n"+
				   "D=M\n";
			}else{
				s= "@THAT\n"+
				   "D=M\n";
			}
		}else if(seg.equals("constant")){
			s= "@"+loc+"\n"+
			   "D=A\n";
		}else if(seg.equals("static")){
			s= "@STATIC"+loc+"\n"+//CHANGE
			   "D=M\n";
		}else if(seg.equals("temp")){
			s= "@R5\n"   +
				"D=A\n"  +
				"@"+loc+"\n"+
				"A=D+A\n"+
				"D=M\n";
		}

		s+= "@SP\n" +
		    "A=M\n" +
		    "M=D\n" +
		    "@SP\n" +
		    "M=M+1\n";
		
		return s;
	}
	
	public String pop(String seg, String loc){
		String s ="";
		if(seg.equals("local")){
			s= "@LCL\n" +
			   "D=M\n"  +
			   "@"+loc+"\n" +
			   "D=D+A\n";		   
		}else if(seg.equals("argument")){
			s= "@ARG\n"+
			   "D=M\n"+
			   "@"+loc+"\n" +
			   "D=D+A\n";		   
		}else if(seg.equals("this")){
			s= "@THIS\n"+
			   "D=M\n"+
			   "@"+loc+"\n" +
			   "D=D+A\n";
		}else if(seg.equals("that")){
			s= "@THAT\n"+
			   "D=M\n"+
			   "@"+loc+"\n" +
			   "D=D+A\n";
		}else if(seg.equals("pointer")){
			if(loc.equals("0")){
				s= "@THIS\n"+
				   "D=A\n"  ;
			}else{
				s= "@THAT\n"+
				   "D=A\n";
			}
		}else if(seg.equals("static")){
			s= "@STATIC"+loc+"\n"+
			   "D=A\n";
		}else if(seg.equals("temp")){
			s= "@R5\n"   +
				"D=A\n"  +
				"@"+loc+"\n"+
				"D=D+A\n";
		}else if(seg.equals("constant")) {
			System.out.println("Semantic error: cannot pop a constant");
			System.exit(0);
		}
		// add generic POP ending
		s+= "@R13\n"  +
		    "M=D\n"   +
		    "@SP\n"   +
		    "M=M-1\n" +
		    "A=M\n"   +
		    "D=M\n"   +
		    "@R13\n"  +
		    "A=M\n"   +
		    "M=D\n";
		return s;
	}
	
	public String add(){
		String s = "@SP\n"  +		
				   "M=M-1\n"+	//M[SP]=255
				   "A=M\n"  +	//Areg=255
				   "D=M\n"  +   //d=M[255]
				   "A=A-1\n"+	//Areg =254
				   "M=D+M\n";	//M[254]=M[255]+M[254]
		return s;
	}
	
	public String sub(){
		String s = "@SP\n"  + 
				   "M=M-1\n"+ // move SP down 1 (final position pointing above answer)
				   "A=M\n"  + // Areg = 255
				   "D=M\n"  + // D=M[255]
				   "A=A-1\n"+ // Areg=255-1
				   "M=M-D\n"; // M[254]=M[254]-M[255]
		return s;
	}
	public String neg(){
		String s = "@SP\n"  +
				   "A=M-1\n"+ // Access M 1 below SP*
				   "M=-M\n";  // Dreg
		return s;
	}
	public String and(){
		String s = "@SP\n"  +
				   "M=M-1\n"+
				   "A=M\n"  +
				   "D=M\n"  +
				   "A=A-1\n"+
				   "M=M&D\n";
		return s;
	}
	public String or(){
		String s = "@SP\n"  +
				   "M=M-1\n"+
				   "A=M\n"  +
				   "D=M\n"  +
				   "A=A-1\n"+
				   "M=M|D\n";
		return s;
	}	
	public String not(){
		String s = "@SP\n"  +
				   "A=M-1\n"+
				   "D=!M\n" ;
		return s;
	}
	public String eq(){
		String count = nextCount();
		String s = "@SP\n"  +
				   "M=M-1\n"+
				   "A=M\n"  +
				   "D=M\n"  +
				   "A=A-1\n"+
				   "D=D-M\n"+
				   "@EQTRUE"+count+"\n"+
				   "D;JEQ\n"+
				   "@SP\n"  +
				   "A=M-1\n"+
				   "M=0\n" +
				   "@EQAFTER"+count+"\n"+
				   "0;JMP\n"+
				   "(EQTRUE"+count+")\n"+
				   "@SP\n"  +
				   "A=M-1\n"+
				   "M=-1\n"  +
				   "(EQAFTER"+count+")\n";
		return s;
	}
	public String lt(){
		String count = nextCount();
		String s = "@SP\n"  +
				   "M=M-1\n"+
				   "A=M\n"  +
				   "D=M\n"  +
				   "A=A-1\n"+
				   "D=M-D\n"+
				   "@LTTRUE"+count+"\n" +
				   "D;JLT\n"+
				   "@SP\n"  +
				   "A=M-1\n"+
				   "M=0\n" +
				   "@LTSKIP"+count+"\n" +
				   "0;JMP\n"+
				   "(LTTRUE"+count+")\n"+
				   "@SP\n"  +
				   "A=M-1\n"+
				   "M=-1\n"  +
				   "(LTSKIP"+count+")\n";
		return s;
	}
	public String gt(){
		String count = nextCount();
		String s = "@SP\n"  +
				   "M=M-1\n"+
				   "A=M\n"  +
				   "D=M\n"  +
				   "A=A-1\n"+
				   "D=M-D\n"+
				   "@GTTRUE"+count+"\n" +
				   "D;JGT\n"+
				   "@SP\n"  +
				   "A=M-1\n"+
				   "M=0\n" +
				   "@GTSKIP"+count+"\n" +
				   "0;JMP\n"+
				   "(GTTRUE"+count+")\n"+
				   "@SP\n"  +
				   "A=M-1\n"+
				   "M=-1\n"  +
				   "(GTSKIP"+count+")\n";
		return s;
	}
	
	public String ifGoTo(String funcName, String label){
		String s = "@SP\n"  +
				   "M=M-1\n"+
				   "A=M\n"  +
				   "D=M\n"  +
				   "@"+funcName+label+"\n"  +
				   "D;JNE\n";
		return s;  
	}
	
	public String goTo(String funcName, String label){
		String s = "@"+funcName+label+"\n"+
				   "0;JMP\n";
		return s;
	}
	
	public String label(String funcName, String label){
		String s = "("+funcName+label+")\n";
		return s;
	}
}
