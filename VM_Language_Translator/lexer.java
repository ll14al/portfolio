import java.io.IOException;

public class lexer {
 
	public static void main(String[] args) throws IOException {
	    
		RDP rdp = new RDP("C:\\Users\\Home\\workspace\\VM_Language_Translator\\src\\codefile");
		System.out.println(rdp.generated_Code);
		
		Lexical_Analyser la = new Lexical_Analyser("C:\\Users\\Home\\workspace\\VM_Language_Translator\\src\\codefile");
		Token t1 = la.getNextToken();
		
		while(t1.type!=TokenType.EOF){
			t1.printToken();
			t1 = la.getNextToken();
		}
		
	}
}
