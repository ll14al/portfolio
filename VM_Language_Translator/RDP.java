import java.io.IOException;


public class RDP {

	Lexical_Analyser la;
	Code_Generator cg;
	String generated_Code;
	
	public RDP(String filename) throws IOException {
		cg = new Code_Generator();
		la = new Lexical_Analyser(filename);
		generated_Code = VM_PROG();
	}
	
	public String VM_PROG() throws IOException {
		return VM_COM_SEQ();
	}
	
	public String VM_COM_SEQ() throws IOException {
		
		Token t = la.getNextToken();
		String output = cg.init()+VM_COM(t);
		boolean i=true;
		while(i){
			t=la.getNextToken();
			if(t.type==TokenType.EOF){
				i=false;
			}
			output += VM_COM(t);
		}
		return output;
	}
	
	public String VM_COM(Token t) throws IOException{
		
		String s = t.lexeme;
		if(t.arithmeticLogic.contains(s))  return AL_COM(t);
		else if(t.memAccess.contains(s))   return MEM_COM(t);
		else if(t.functionComs.contains(s))return FUN_COM(t);
		else if(t.programFlow.contains(s)) return PF_COM(t);
		else return" ";
		}
	
	
	public String AL_COM(Token t){
		String s = t.lexeme;
		if(s.equals("add"))return cg.add();
		if(s.equals("sub"))return(cg.sub());
		if(s.equals("eq")) return(cg.eq());
		if(s.equals("gt")) return(cg.gt());
		if(s.equals("lt")) return(cg.lt());
		if(s.equals("and"))return(cg.and());
		if(s.equals("or")) return(cg.or());
		if(s.equals("not"))return(cg.not());
		if(s.equals("neg"))return(cg.neg());
		else {
			System.out.println("malformed arithmetic or logic command");
			System.exit(0);
			return "";
		}
	}
	
	public String MEM_COM(Token t) throws IOException{
		String s = t.lexeme;
		if(s.equals("push"))return MEM_SEG("push");
		if(s.equals("pop")) return MEM_SEG("pop");
		else {
			System.out.println("malformed memory access command");
			System.exit(0);
			return "";
		}
	}
	
	public String MEM_SEG(String p) throws IOException{
		Token t1 = la.getNextToken();
		String memseg = t1.lexeme; //
		Token t2 = la.getNextToken();
		String loc = t2.lexeme;
		
		if(t1.memAccess.contains(memseg)&&t2.type==TokenType.INT){// if memory segment followed by int
			if(p.equals("push"))return cg.push(memseg,loc);
			if(p.equals("pop")) return(cg.pop(memseg,loc));
			
		}else{
			System.out.println("error memory access command wrong");
			System.exit(0);
		}
		return "malformed memory command";
	}
	
	public String PF_COM(Token t) throws IOException{
		if(t.lexeme.equals("goto")){
			t=la.getNextToken();
			if(t.type==TokenType.NAME){
				return cg.goTo(cg.funcName, t.lexeme);
			}else{
				System.out.println("incorrect syntax: expexted NAME");
				System.exit(0);
			}
		}else if(t.lexeme.equals("if-goto")) {
			t=la.getNextToken();
			if(t.type==TokenType.NAME){
				return cg.ifGoTo(cg.funcName, t.lexeme); 	
			}else{
				System.out.println("incorrect syntax: expexted NAME");
				System.exit(0);
			}
		}else if(t.lexeme.equals("label")) {
			t=la.getNextToken();
			if(t.type==TokenType.NAME){
				return cg.label(cg.funcName, t.lexeme); 	
			}else{
				System.out.println("incorrect syntax: expexted NAME");
				System.exit(0);
			}
		}
		System.out.println("incorrect syntax: expexted NAME");
		System.exit(0);
		return "";
	}

	public String FUN_COM(Token t) throws IOException{
		if(t.lexeme.equals("function")){
			return FUN_DEC();	
		}else if(t.lexeme.equals("call")){
			return FUN_CALL();	
		}else if(t.lexeme.equals("return")){
			return cg.returnFucntion();
		}
		return "";
	}
	
	public String FUN_DEC() throws IOException{
		Token t = la.getNextToken();
		cg.funcName=t.lexeme;
		if(t.type==TokenType.NAME){
			t=la.getNextToken();
			if(t.type==TokenType.INT){
				return cg.fucntionDec(cg.funcName, t.lexeme);
			}else{
				System.out.println("incorrect syntax: expected INT");
				return"";
			}
		}else{
			System.out.println("incorrect syntax: expected NAME");
			System.exit(0);
			return"";
		}
	
	}
	
	public String FUN_CALL() throws IOException{
		Token t = la.getNextToken();
		if(t.type==TokenType.NAME){
			return cg.callFunction(t.lexeme, cg.count);
		}else{
			System.out.println("incorrect syntax: expected NAME");
			System.exit(0);
			return "";
		}
	}
	
	public boolean INT() throws IOException{// is next token int
		Token t = la.getNextToken();
		if(t.type==TokenType.INT){
			return true;
		}else{
			System.out.println("Expected INT");
			System.exit(0);
			return false;
		}
	}
}
