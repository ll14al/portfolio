import java.util.Arrays;
import java.util.LinkedList;

public class Token
{
	protected String lexeme = new String();  //
	protected TokenType type; //
	protected LinkedList<String> keywords = new LinkedList<String>(Arrays.asList("push","pop","add",
			"sub","neg","eq","gt","lt","and","or","not","constant","local","argument","static","this",
			"that","pointer","temp","label","goto","if-goto","function","call","return"));
	
	protected LinkedList<String> arithmeticLogic = new LinkedList<String>(Arrays.asList("add",
			"sub","neg","eq","gt","lt","and","or","not"));
	
	protected LinkedList<String> memAccess = new LinkedList<String>(Arrays.asList("push","pop","constant",
			"local","argument","static","this","that","pointer","temp"));
	
	protected LinkedList<String> programFlow = new LinkedList<String>(Arrays.asList("label","goto",
			"if-goto"));
	
	protected LinkedList<String> functionComs = new LinkedList<String>(Arrays.asList("function","call",
			"return"));
	
	public Token(){
		
	}
	
	public Token( String inputlex ){
		// string constructor
		lexeme = inputlex; // set lexeme
		for(int i=0; i<keywords.size();i++){ // if lexeme is a keyword, set as type
			if(lexeme.equals(keywords.get(i))){
				type=TokenType.KEYWORD;
				break;
			}
			if(lexeme.equals("EOF")){
				type=TokenType.EOF;
			}
			else{ // if not a keyword must be a label
				type=TokenType.NAME;
			}
		}	
	}
	
	public Token( int num ){
		// int constructor
		String inputlex = Integer.toString(num);
		lexeme = inputlex;
		type= TokenType.INT;
	}
	
	public void printToken(){
		System.out.println("Lexeme: "+lexeme+"  Type: "+type);
	}
}